﻿using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace Oracle.Identity.Dapper
{
    /// <summary>
    /// Class that represents the UserRoles table in the Database
    /// </summary>
    public class UserRolesTable
    {
        private DbManager db;

        /// <summary>
        /// Constructor that takes a DbManager instance 
        /// </summary>
        /// <param name="database"></param>
        public UserRolesTable(DbManager database)
        {
            db = database;
        }

        /// <summary>
        /// Returns a list of user's roles
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public List<string> FindByUserId(int OperatorId)
        {
            return db.Connection.Query<string>("Select Name from operatorrole where id=:OperatorId", new{ OperatorId} )
                .ToList();
        }

        /// <summary>
        /// Deletes all roles from a user in the UserRoles table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public void Delete(int OperatorId)
        {
            db.Connection.Execute(@"Delete from operatorrole where Id = :OperatorId", new {OperatorId });
        }

        /// <summary>
        /// Inserts a new role for a user in the UserRoles table
        /// </summary>
        /// <param name="user">The User</param>
        /// <param name="roleId">The Role's id</param>
        /// <returns></returns>
        public void Insert(IdentityMember member, int roleId)
        {
            db.Connection.Execute(@"Insert into operatorrole (id, name) values :userId, :roleId",
                new { userId = member.Id, roleId });
        }
    }
}
