﻿namespace Oracle.Identity.Dapper
{
    public class OperatorClaim
    {
        public int Id { get; set; }

        public int OperatorId { get; set; }

        public string ClaimType { get; set; }

        public string ClaimValue { get; set; }
    }
}
