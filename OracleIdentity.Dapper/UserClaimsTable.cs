﻿using System.Linq;
using Dapper;
using System.Security.Claims;

namespace Oracle.Identity.Dapper
{
    /// <summary>
    /// Class that represents the UserClaims table in the Database
    /// </summary>
    public class UserClaimsTable
    {
        private DbManager db;

        /// <summary>
        /// Constructor that takes a DbManager instance 
        /// </summary>
        /// <param name="database"></param>
        public UserClaimsTable(DbManager database)
        {
            db = database;
        }

        /// <summary>
        /// Returns a ClaimsIdentity instance given a OperatorId
        /// </summary>
        /// <param name="OperatorId">The user's id</param>
        /// <returns></returns>
        public ClaimsIdentity FindByOperatorId(int Id)
        {
           ClaimsIdentity claims = new ClaimsIdentity();

           foreach (var c in db.Connection.Query<OperatorClaim>("Select * from OperatorClaim where operatorId=:Id", new { Id }).ToList())
           {
               claims.AddClaim(new Claim(c.ClaimType, c.ClaimValue));
           }

           return claims;
        }

        /// <summary>
        /// Deletes all claims from a user given a OperatorId
        /// </summary>
        /// <param name="OperatorId">The user's id</param>
        /// <returns></returns>
        public void Delete(int Id)
        {
            db.Connection.Execute(@"Delete from OperatorClaim where OperatorId = :Id", new { Id = Id });
        }

        /// <summary>
        /// Inserts a new claim in UserClaims table
        /// </summary>
        /// <param name="OperatorClaim">User's claim to be added</param>
        /// <param name="OperatorId">User's id</param>
        /// <returns></returns>
        public void Insert(Claim OperatorClaim, int Id)
        {
            db.Connection.Execute(@"Insert into OperatorClaim (ClaimValue, ClaimType, Id) 
                values (:value, :type, :OperatorId)", 
                    new { 
                        value=OperatorClaim.Value,
                        type=OperatorClaim.Type,OperatorId=Id
                        });
        }

        /// <summary>
        /// Deletes a claim from a user 
        /// </summary>
        /// <param name="user">The user to have a claim deleted</param>
        /// <param name="claim">A claim to be deleted from user</param>
        /// <returns></returns>
        public void Delete(IdentityMember member, Claim claim)
        {
            db.Connection.Execute(@"Delete from OperatorClaim 
            where OperatorId = :Id and :ClaimValue = :value and ClaimType = :type",
                new { 
                    Id = member.Id,
                    ClaimValue=claim.Value,
                    type=claim.Type 
                });
        }
    }
}
