﻿using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Oracle.Identity.Dapper
{
    /// <summary>
    /// Class that represents the Users table in the Database
    /// </summary>
    public class UserTable<TUser>
        where TUser : IdentityMember
    {
        private DbManager db;

        /// <summary>
        /// Constructor that takes a DbManager instance 
        /// </summary>
        /// <param name="database"></param>
        public UserTable(DbManager database)
        {
            db = database;
        }

        /// <summary>
        /// Returns the Member's name given a Operator id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public string GetUserName(int Id)
        {
            return db.Connection.ExecuteScalar<string>("Select Name from Operator where Id=:Id", new {Id });
        }

        /// <summary>
        /// Returns a Operator ID given a Operator name
        /// </summary>
        /// <param name="userName">The Member's name</param>
        /// <returns></returns>
        public int GetId(string userName)
        {
            return db.Connection.ExecuteScalar<int>("Select Id from Operator where UserName=:UserName", new { UserName=userName });
        }

        /// <summary>
        /// Returns an TUser given the Member's id
        /// </summary>
        /// <param name="Id">The Member's id</param>
        /// <returns></returns>
        public TUser GetUserById(int Id)
        {
            return db.Connection.Query<TUser>("Select * from Operator where Id=:Id", new {Id })
                .FirstOrDefault();
        }

        /// <summary>
        /// Returns a list of TUser instances given a Operator name
        /// </summary>
        /// <param name="userName">Member's name</param>
        /// <returns></returns>
        public List<TUser> GetUserByName(string userName)
        {
            return db.Connection.Query<TUser>("Select * from Operator where UserName=:UserName", new { UserName=userName })
                .ToList();
        }

        public TUser GetUserByEmail(string email)
        {
            return db.Connection.Query<TUser>("Select * from Operator where Email=:Email", new { Email = email })
                .FirstOrDefault();
        }

        /// <summary>
        /// Return the Member's password hash
        /// </summary>
        /// <param name="Id">The Member's id</param>
        /// <returns></returns>
        public string GetPasswordHash(int Id)
        {
            return db.Connection.ExecuteScalar<string>("Select PasswordHash from Operator where Id = :Id", new {Id});
        }

        /// <summary>
        /// Sets the Member's password hash
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        public void SetPasswordHash(int Id, string passwordHash)
        {
            db.Connection.Execute(@"
                    UPDATE
                        Operator
                    SET
                        PasswordHash = :pwdHash
                    WHERE
                        Id = :Id", new { pwdHash = passwordHash, Id });
        }

        /// <summary>
        /// Returns the Member's security stamp
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public string GetSecurityStamp(int Id)
        {
            return db.Connection.ExecuteScalar<string>("Select SecurityStamp from Operator where Id = :Id", new {Id });
        }

        /// <summary>
        /// Inserts a new Operator in the Users table
        /// </summary>
        /// <param name="Member"></param>
        /// <returns></returns>
        public void Insert(TUser member)
        {
           var id = db.Connection.ExecuteScalar<int>(@"Insert into Operator
                                    (UserName, FirstName, Surname, Gender, DateCreated, PasswordHash, SecurityStamp,Email,EmailConfirmed,PhoneNumber,LockoutEnabled)
                            values  (:name, :FirstName, :Surname, :Gender, :DateCreated, :pwdHash, :SecStamp,:email,:emailconfirmed,:phonenumber,:lockoutenabled)
                            SELECT Cast(SCOPE_IDENTITY() as int)",
                             new {  
                                    name=member.UserName,
                                    pwdHash=member.PasswordHash,
                                    firstname=member.FirstName,
                                    surname=member.Surname,
                                    gender=member.Gender,
                                    datecreated=member.DateCreated,
                                    SecStamp=member.SecurityStamp,
                                    email=member.Email,
                                    emailconfirmed=member.EmailConfirmed,
                                    phonenumber=member.PhoneNumber,
                                    lockoutenabled=member.LockoutEnabled
                             });
            // we need to set the id to the returned identity generated from the db
            member.Id = id;
        }

        /// <summary>
        /// Deletes a Operator from the Users table
        /// </summary>
        /// <param name="Id">The Member's id</param>
        /// <returns></returns>
        private void Delete(int Id)
        {
            db.Connection.Execute(@"Delete from Operator where Id = :Id", new {Id });
        }

        /// <summary>
        /// Deletes a Operator from the Users table
        /// </summary>
        /// <param name="Member"></param>
        /// <returns></returns>
        public void Delete(TUser Member)
        {
            Delete(Member.Id);
        }

        /// <summary>
        /// Updates a Operator in the Users table
        /// </summary>
        /// <param name="Member"></param>
        /// <returns></returns>
        public void Update(TUser member)
        {
            var result = db.Connection
              .ExecuteScalar<int>(@"
                            Update Operator set UserName = :userName, surname = :surname,  PasswordHash = :pswHash, Firstname = :firstname, Gender = :gender, 
                            Email =:email, SecurityStamp = :secStamp, Email_Confirmed =:emailconfirmed, PhoneNumber =:phonenumber WHERE Id = :Id",
                //Update Operator set UserName = :userName, PasswordHash = :pswHash, SecurityStamp = :secStamp, Firstname = :firstname, Surname = :surname, Gender = :gender,
                //Email =:email, EmailConfirmed =:emailconfirmed, PhoneNumber =:phonenumber, LockoutEnabled =:lockoutenabled WHERE Id = :Id
                new
                {
                    userName= member.UserName,
                    pswHash= member.PasswordHash,
                    firstname = member.FirstName,
                    surname = member.Surname,
                    gender = member.Gender,
                    secStamp = member.SecurityStamp,
                    member.Id,
                    email= member.Email,
                    emailconfirmed= member.EmailConfirmed ? 1 : 0,
                    phonenumber= member.PhoneNumber,
                    lockoutenabled= member.LockoutEnabled ? 1 : 0,
                }            
           );
        }
    }
}
