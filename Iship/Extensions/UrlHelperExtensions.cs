using Iship.Controllers;
using System.Web.Mvc;

namespace Iship.Extensions
{
    public static class UrlHelperExtensions
    {
        public static string ResetPasswordCallbackLink(this UrlHelper urlHelper, string userId, string code, string scheme)
        {
            return urlHelper.Action(
                actionName: nameof(AccountController.ResetPassword),
                controllerName: "Account",
                routeValues: new { userId, code },
                protocol: scheme);
        }
    }
}
