﻿namespace Iship.ViewModels
{
    public class Kpi
    {
        public string Status { get; set; }

        public string Name { get; set; }

        public string ViewUrl { get; set; }

        public string Value1 { get; set; }

        public string Value2 { get; set; }
    }
}