﻿using Iship.Models;
using System.Collections.Generic;

namespace Iship.ViewModels
{
    public class EditSubclassWrap
    {
        public string SubclassId { get; set; }

        public List<SubSection> Subclasses { get; set; }
    }
}
