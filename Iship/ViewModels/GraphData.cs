﻿using System.Collections.Generic;

namespace Iship.ViewModels
{
    public class GraphData
    {
        public GraphData()
        {
            X = new List<string>();
            Y = new List<int>();
        }

        public List<string> X { get; set; }

        public List<int> Y { get; set; }
    }
}