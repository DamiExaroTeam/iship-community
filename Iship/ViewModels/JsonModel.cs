﻿namespace Iship.ViewModels
{
    public class JsonModel
    {
        public string name { get; set; }

        public string value { get; set; }

        public string text { get; set; }

        public string disabled { get; set; }
    }
}