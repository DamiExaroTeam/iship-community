﻿namespace Iship.ViewModels
{
    public class Branch
    {
        public string BranchCode { get; set; }

        public int BranchId { get; set; }

        public string BranchName { get; set; }

        public string Ip { get; set; }

        public string conn { get; set; }
    }
}
