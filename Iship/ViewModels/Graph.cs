﻿namespace Iship.ViewModels
{
    public class Graph
    {
        public string Name { get; set; }

        public string Title { get; set; }

        public string Title2 { get; set; }
        
        public int SectionId { get; set; }

        public string Section { get; set; }

        public string XAxis { get; set; }

        public string YAxis { get; set; }

        public string Color { get; set; }

        public string Labels { get; set; }

        public string Series { get; set; }

        public int High { get; set; }
    }
}