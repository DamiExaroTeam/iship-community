﻿using Iship.Models;
using System.Collections.Generic;

namespace Iship.ViewModels
{
    public class MenuModel
    {
        public string Username { get; set; }

        public List<UserClaim> Claims { get; set; }

        public List<Section> Sections { get; set; }

        public Dashboard Dashboard { get; set; }
    }
}
