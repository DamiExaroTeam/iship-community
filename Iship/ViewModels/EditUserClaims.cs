﻿using System.Collections.Generic;

namespace Iship.ViewModels
{
    public class EditUserClaims
    {
        public EditUserClaims()
        {
            SelectedClaims = new List<string>();
            UnSelectedClaims = new List<string>();
        }

        public string Controller { get; set; }

        public List<string> SelectedClaims { get; set; }

        public List<string> UnSelectedClaims { get; set; }
    }
}
