﻿using Iship.Models;
using System.Collections.Generic;

namespace Iship.ViewModels
{
    public class EditSubclass
    {
        public int Id { get; set; }

        public string SubclassName { get; set; }

        public List<UserClaim> SelectedClaims { get; set; }

        public List<UserClaim> UnSelectedClaims { get; set; }
    }
}
