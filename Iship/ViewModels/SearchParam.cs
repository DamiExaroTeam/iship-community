﻿namespace Iship.ViewModels
{
    public class SearchParam
    {
        public int PerPage { get; set; }

        public string SearchName { get; set; }

        public string SearchLga { get; set; }

        public string SearchStartDate { get; set; }

        public string SearchEndDate { get; set; }

        public string SearchCommunity { get; set; }

        public string Firstname { get; set; }

        public string Surname { get; set; }

        public string CustomTitle { get; set; }

        public string SearchGender { get; set; }

        public string SearchState { get; set; }

        public string Reverse { get; set; }

        public int Page { get; set; } = 0;

        public int IsPrint { get; set; } = 0;

        public string OrderBy { get; set; }

        public int PrintPage { get; set; } = 0;
    }
}