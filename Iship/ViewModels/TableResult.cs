﻿using System.Collections.Generic;

namespace Iship.ViewModels
{
    public class TableResult
    {
        public TableResult()
        {
            Headers = new List<string>();
            Rows = new List<List<string>>();
        }
        public string Title { get; set; }

        public List<string> Headers { get; set; }

        public List<List<string>> Rows { get; set; }

    }
}