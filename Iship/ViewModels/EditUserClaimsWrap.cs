﻿using System.Collections.Generic;

namespace Iship.ViewModels
{
    public class EditUserClaimsWrap
    {
        public string UserId { get; set; }

        public string Controller { get; set; }

        public List<EditUserClaims> Claims { get; set; }
    }
}
