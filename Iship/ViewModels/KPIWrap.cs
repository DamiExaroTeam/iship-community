﻿using System.Collections.Generic;

namespace Iship.ViewModels
{
    public class KpiWrap
    {
        public string Title { get; set; }

        public int SectionId { get; set; }

        public List<Kpi> Kpis { get; set; }

        public bool IsGraph { get; set; }

        public Graph Graph { get; set; }

        public bool DisableView { get; set; }
    }
}