﻿using Iship.Extensions;
using System;
using System.ComponentModel.DataAnnotations;

namespace Iship.ViewModels
{
    public class UserRequest
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        [Required]
        [StringLength(20)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20)]
        public string Surname { get; set; }

        public string RegistrationCode { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(50)]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [StringLength(100)]
        public string Message { get; set; }

        public bool IsActive { get; set; }

        public DateTime DateCreated { get; set; } = DateTime.Now;

        public string GetFullName()
        {
            return FirstName.ToTitleCase() + " " + Surname.ToTitleCase();
        }
    }
}
