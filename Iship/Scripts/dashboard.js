﻿function Dashboard() {
    $(".card-header").each(function () {
        var name = $(this).data("graphname");
        var high = $(this).data("high");
        var series = $(this).data("series");
        var labels = $(this).data("labels");
        var xaxis = $(this).data("xaxis");
        var yaxis = $(this).data("yaxis");

        var graphData = {
            labels: labels.split(","), 
            series: [series.split(",")] 
        };

        var graphOptions = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            height: "250px",
            high: high,
            chartPadding: {
                top: 20,
                right: 0,
                bottom: 20,
                left: 10
            },
            axisX: {
                
            },
            axisY: {
                labelOffset: {
                    x: 0,
                    y: 0
                }
            },
            plugins: [
                Chartist.plugins.ctAxisTitle({
                    axisX: {
                        axisTitle: xaxis,
                        axisClass: "ct-axis-title",
                        offset: {
                            x: -20,
                            y: 45
                        },
                        textAnchor: "middle"
                    },
                    axisY: {
                        axisTitle: yaxis,
                        axisClass: "ct-axis-title",
                        offset: {
                            x: 0,
                            y: 0
                        },
                        textAnchor: "middle",
                        flipTitle: true
                    }
                })
            ]
        }

        var chart = new Chartist.Line("#" + name, graphData, graphOptions);

        // Let's put a sequence number aside so we can use it in the event callbacks
        var seq = 0,
            delays = 10,
            durations = 500;

        // Once the chart is fully created we reset the sequence
        chart.on("created", function () {
            seq = 0;
        });

        // On each drawn element by Chartist we use the Chartist.Svg API to trigger SMIL animations
        chart.on("draw", function (data) {
            seq++;

            if (data.type === "line") {
                // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
                data.element.animate({
                    opacity: {
                        // The delay when we like to start the animation
                        begin: seq * delays + 1000,
                        // Duration of the animation
                        dur: durations,
                        // The value where the animation should start
                        from: 0,
                        // The value where it should end
                        to: 1
                    }
                });
            } else if (data.type === "label" && data.axis === "x") {
                data.element.animate({
                    y: {
                        begin: seq * delays,
                        dur: durations,
                        from: data.y + 100,
                        to: data.y,
                        // We can specify an easing function from Chartist.Svg.Easing
                        easing: "easeOutQuart"
                    }
                });
            } else if (data.type === "label" && data.axis === "y") {
                data.element.animate({
                    x: {
                        begin: seq * delays,
                        dur: durations,
                        from: data.x - 100,
                        to: data.x,
                        easing: "easeOutQuart"
                    }
                });
            } else if (data.type === "point") {
                data.element.animate({
                    x1: {
                        begin: seq * delays,
                        dur: durations,
                        from: data.x - 10,
                        to: data.x,
                        easing: "easeOutQuart"
                    },
                    x2: {
                        begin: seq * delays,
                        dur: durations,
                        from: data.x - 10,
                        to: data.x,
                        easing: "easeOutQuart"
                    },
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: "easeOutQuart"
                    }
                });
            } else if (data.type === "grid") {
                // Using data.axis we get x or y which we can use to construct our animation definition objects
                var pos1Animation = {
                    begin: seq * delays,
                    dur: durations,
                    from: data[data.axis.units.pos + "1"] - 30,
                    to: data[data.axis.units.pos + "1"],
                    easing: "easeOutQuart"
                };

                var pos2Animation = {
                    begin: seq * delays,
                    dur: durations,
                    from: data[data.axis.units.pos + "2"] - 100,
                    to: data[data.axis.units.pos + "2"],
                    easing: "easeOutQuart"
                };

                var animations = {};
                animations[data.axis.units.pos + "1"] = pos1Animation;
                animations[data.axis.units.pos + "2"] = pos2Animation;
                animations["opacity"] = {
                    begin: seq * delays,
                    dur: durations,
                    from: 0,
                    to: 1,
                    easing: "easeOutQuart"
                };

                data.element.animate(animations);
            }
        });
    });

    $("#workspace").fadeIn();
}

(function (window, document, Chartist) {
    "use strict";

    function FixedScaleAxis(axisUnit, data, chartRect, options) {
        var highLow = Chartist.getHighLow(data.normalized, options, axisUnit.pos);
        this.divisor = options.divisor || 1;
        this.ticks = options.ticks || Chartist.times(this.divisor).map(function (value, index) {
            return highLow.low + (highLow.high - highLow.low) / this.divisor * index;
        }.bind(this));
        this.range = {
            min: highLow.low,
            max: highLow.high
        };

        Chartist.FixedScaleAxis.super.constructor.call(this,
            axisUnit,
            chartRect,
            this.ticks,
            options);

        this.stepLength = this.axisLength / this.divisor;
    }

    function AutoScaleAxis(axisUnit, data, chartRect, options) {
        // Usually we calculate highLow based on the data but this can be overriden by a highLow object in the options
        var highLow = options.highLow || Chartist.getHighLow(data, options, axisUnit.pos);
        this.bounds = Chartist.getBounds(chartRect[axisUnit.rectEnd] - chartRect[axisUnit.rectStart], highLow, options.scaleMinSpace || 20, options.onlyInteger);
        this.range = {
            min: this.bounds.min,
            max: this.bounds.max
        };

        Chartist.AutoScaleAxis.super.constructor.call(this,
            axisUnit,
            chartRect,
            this.bounds.values,
            options);
    }

    function projectValue(value) {
        return this.axisLength * (+Chartist.getMultiValue(value, this.units.pos) - this.bounds.min) / this.bounds.range;
    }

    Chartist.AutoScaleAxis = Chartist.Axis.extend({
        constructor: AutoScaleAxis,
        projectValue: projectValue
    });

}(window, document, Chartist));