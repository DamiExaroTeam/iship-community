﻿var msgType = {
    Error: 1,
    LoggedOut: 2
};

var errorHandled = false;

$(document)
        .ready(function () {

            // prevent ajax caching
            $.ajaxSetup({ cache: false });

            $.fn.api.settings.successTest = function (response) {
                return response.success == 1;
            }

            // fix menu when passed
            $(".masthead")
                    .visibility({
                        once: false,
                        onBottomPassed: function () {
                            $(".fixed.menu").transition("fade in");
                        },
                        onBottomPassedReverse: function () {
                            $(".fixed.menu").transition("fade out");
                        }
                    })
                ;

            // create sidebar and attach to menu open
            $(".ui.sidebar")
                    .sidebar()
                ;

            // show login
            $(".showlogin, .backtologin").click(function () {
                $(".regdiv").each(function () {
                    $(this).fadeOut();
                });

                $(".regdiv").promise().done(function () {
                    $("#logindiv").fadeIn();
                });
            });

            // show homepage
            $("#bigtitle").click(function () {
                if ($("#defaultdiv").length) {
                    $("#defaultdiv").siblings().each(function() {
                        $(this).fadeOut();
                    });

                    $("#defaultdiv").siblings().promise().done(function() {
                        $("#defaultdiv").fadeIn();
                    });
                } else {
                    window.location.href = "/";
                }
            });

            // reset password
            $(".forgotpw").click(function () {
                $(".regdiv").each(function () {
                    $(this).fadeOut();
                });

                $(".regdiv").promise().done(function () {
                    $("#forgotdiv").fadeIn();
                });
            });

            // initialize accordions and dropdowns
            $(".ui.accordion")
                .accordion();

            $(".ui.dropdown")
                    .dropdown();

            // NOTE: This validation really is not practical for MVC seeing as I can't use my normal model validation.
            // i think it would work great with a javascript framework though.
            // tbh I probably could get this to work if I actually bothered to think about it. Maybe come back to it later? TODO!!

           // login form
           $("#loginfrm")
                .form({
                    className: { success: "" },
                    fields: {
                        email: {
                            identifier: "email",
                            rules: [
                                {
                                    type: "empty",
                                    prompt: "Please enter your e-mail"
                                }
                            ]
                        },
                        password: {
                            identifier: "password",
                            rules: [
                                {
                                    type: "empty",
                                    prompt: "Please enter your password"
                                }
                            ]
                        }
                    }
                })
                .api({
                    serializeForm: true,
                    method: "POST",
                    onSuccess: function (response) {
                        $(this).addClass("success");

                        if (response.isfirsttime) {
                            $(".regdiv").each(function() {
                                $(this).fadeOut();
                            });

                            $(".regdiv").promise().done(function () {
                                $("#FirstTimeId").val(response.userid);
                                $("#firsttimediv").fadeIn();
                            });
                        } else {
                            $(".submit.button").addClass("disabled");
                            setTimeout(
                                function() {
                                    // Hide header and show menu
                                    $("#logindiv").fadeOut(function() {
                                        $(".easy").slideUp();
                                        $("#mainheader").slideUp(function() {
                                            $("#sideMenu").hide().html(response.menu).show();
                                            $("#renderBody").html(response.homepage);
                                            $("#sideMenuSlide")
                                                .transition("slide right").css("height", "100%");

                                            // initialize accordions
                                            $(".ui.accordion")
                                                .accordion();

                                            // get dashboard info
                                            $("a.dashboard.sidemenuitem").click();
                                        });
                                    });
                                },
                                2000);
                        }
                    },
                    onFailure: function (response) {
                        if (response.success != 0)
                            showError(null, null, msgType.Error);

                        if (response.error) {
                            $(this).form("add errors", [response.error]);
                        }
                    }
                })
                ;

            // first time login
           $("#changepwfrm")
               .form({
                   className: { success: "" },
                   fields: {
                       OldPassword: {
                           identifier: "OldPassword",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Please enter password sent to your email address"
                               }
                           ]
                       },
                       NewPassword: {
                           identifier: "NewPassword",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Please enter a new password"
                               }
                           ]
                       },
                       ConfirmPassword: {
                           identifier: "ConfirmPassword",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Please retype the new password to confirm"
                               },
                               {
                                   type: "match[NewPassword]",
                                   prompt: "New password and confirmation do not match"
                               }
                           ]
                       }
                   }
               })
               .api({
                   serializeForm: true,
                   method: "POST",
                   onSuccess: function (response) {
                       $(this).addClass("success");
                       $(".submit.button").addClass("disabled");

                       setTimeout(
                           function () {
                               // Hide header and show menu
                               $("#firsttimediv").fadeOut(function () {
                                   $(".easy").slideUp();
                                   $("#mainheader").slideUp(function () {
                                       $("#sideMenu").hide().html(response.menu).fadeIn();
                                       $("#renderBody").html(response.homepage);
                                       $("#sideMenuSlide")
                                           .transition("slide right").css("height", "100%");

                                       // initialize accordions
                                       $(".ui.accordion")
                                           .accordion()
                                           ;

                                       // show starter menu
                                       //$(".startersector").click();
                                       //$(".startersection").next(".item").slideDown();
                                   });
                               });
                           }, 2000);
                   },
                   onFailure: function (response) {
                       if (response.success != 0)
                           showError(null, null, msgType.Error);

                       if (response.error) {
                           $(this).form("add errors", [response.error]);
                       }
                   }
               })
               ;

            // forgot password form
           $("#forgotfrm")
               .form({
                   className: { success: "" },
                   fields: {
                       email: {
                           identifier: "email",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Please enter your e-mail"
                               }
                           ]
                       }
                   }
               })
               .api({
                   serializeForm: true,
                   method: "POST",
                   onSuccess: function (response) {
                       $(this).addClass("success");
                       $(".submit.button").addClass("disabled");

                       setTimeout(
                           function () {
                               // Hide header and show menu
                               $("#forgotdiv").fadeOut(function () {
                                   $("#logindiv").fadeIn();
                               });
                           }, 2000);
                   },
                   onFailure: function (response) {
                       if (response.success != 0)
                           showError(null, null, msgType.Error);

                       if (response.error) {
                           $(this).form("add errors", [response.error]);
                       }
                   }
               })
               ;

            // reset password form
           $("#resetpasswordfrm")
               .form({
                   className: { success: "" },
                   fields: {
                       email: {
                           identifier: "email",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Please enter your e-mail"
                               }
                           ]
                       },
                       password: {
                           identifier: "password",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Enter your new password"
                               },
                               {
                                   type: "minLength[4]",
                                   prompt: "Your password must be at minimum of 4 characters"
                               }
                           ]
                       },
                       confirmpassword: {
                           identifier: "confirmpassword",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Your new password and confirmation password must match"
                               }
                           ]
                       }
                   }
               })
               .api({
                   serializeForm: true,
                   method: "POST",
                   onSuccess: function () {
                       $(this).find(".success.message").fadeIn().delay(2000).fadeOut(function() {
                           window.location.href = "/";
                       });
                   },
                   onFailure: function (response) {
                       if (response.success != 0)
                           showError(null, null, msgType.Error);

                       if (response.error) {
                           $(this).form("add errors", [response.error]);
                       }
                   }
               });

            // catch all 401 errors
            $(document).ajaxError(function (event, jqxhr) {
                event.preventDefault();
                $("form .field.error").removeClass("error");
                $("form .ui.error.message ul").remove();

                if (jqxhr.status == 401 || jqxhr.status == 403) {
                    showError(null, null, msgType.LoggedOut);

                    setTimeout(
                        function () {
                            window.location.href = "/";
                        },
                        2000);
                } else if (jqxhr.status == 408) {
                    showError("Connection attempt timeout", "Could not reach server", null);
                    errorHandled = true;
                } else if (jqxhr.status == 412) {
                    showError("Unauthorized Function", "You have not been given access to this function. Please contact an administrator to acquire access", null);
                } else
                    showError(null, null, msgType.Error);
            });
        })
    ;

function showError(header, message, type) {
    if (errorHandled == true) {
        errorHandled = false;
        return;
    }

    if (header) {
        $("#popupMsg .header").text(header);
    }

    if (message) {
        $("#popupMsg .content p").text(message);
    }

    if (type == msgType.Error) {
        $("#popupMsg .header").text("Error");
        $("#popupMsg .content p").text("An error has occurred. It has been logged. If it persists please contact an admin.");
    }

    if (type == msgType.LoggedOut) {
        $("#popupMsg .header").text("Authorisation Denied");
        $("#popupMsg .content p").text("You have been logged out of the system. Please log in again to continue");
    }

    $("#popupMsg")
            .modal("show")
        ;
}