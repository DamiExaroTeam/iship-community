﻿function Community() {
    setDropdowns();
    
    $(".viewcommunity").click(function() {
        $("#menuloader").addClass("active");
        var controller = $(this).data("section");
        var action = $(this).data("option");
        var subsection = $(this).data("subsection");
        var url = "/" + controller + "/" + action;

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                error: function() {
                    $(".loader").removeClass("active");
                },
                success: function(result) {
                    $(".loader").removeClass("active");
                    $("#workspace").fadeOut(function () {
                        // update sidemenu
                        if (!$("#" + controller + "section").hasClass("selected"))
                            $("#" + controller + "section").click();

                        if (!$("#" + subsection + "subsection").hasClass("active"))
                            $("#" + subsection + "subsection").click();

                        $("#" + subsection + "subsection").next(".content").find("a[data-option='" + action.split("?")[0] + "']").addClass("active");

                        $(this).html(result.page);

                        OnUpdate();

                        $(".dropdown").dropdown();

                       $(this).fadeIn();

                        ActivateSearch();
                    });

                    // update url
                    var loc = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "");
                    history.replaceState("", "PassFix", loc + "/Tools?section=" + controller + "&subsection=" + subsection + "&option=" + action);
                }
            });
    });

    $(".edititem").click(function () {
        var url = "/Community/ViewEditCommunity?id=" + $(this).data("id");

        ShowModal(url, 1, setDropdowns);
    });

    // admin checkbox
    $(".adminCheckbox").click(function () {
        var checkbox = $(this).find("input:checkbox:first");
        var url = $(this).data("url");
        var checked = checkbox.is(":checked");
        if (checked) {
            $("#minimodalcontent").text("Are you sure you want to remove the administrator role from this user?");
            url = url + "0";
        } else {
            $("#minimodalcontent").text("Are you sure you want to make this user an administrator?");
            url = url + "1";
        }

        $(".ui.mini.modal")
                .modal({
                    closable: false,
                    onApprove: function () {
                        $(".positive.button").addClass("loading");
                        $.ajax({
                            type: "POST",
                            url: url,
                            dataType: "json",
                            error: function () {
                                $(".positive.button").removeClass("loading");
                            },
                            success: function (result) {
                                $(".positive.button").removeClass("loading");
                                if (result.success == 1) {
                                    $("#manageOperators").fadeOut(function () {
                                        $(this).html(result.page).fadeIn();
                                        ShowDisabledUsers();
                                    });
                                    $(".negative.button").click();
                                } else
                                    $(this).find(".error.message").fadeIn();
                            }
                        });

                        return false;
                    }
                })
                .modal("show")
            ;
    });
    
    $("#manageDisabledUsers").checkbox({
        onChecked: function () {
            $(".disabledusers").slideDown();


        },
        onUnchecked: function () {
            $(".disabledusers").slideUp();
        }
    });
}

function setDropdowns() {
    $(".lgas, #CommunityId, #Gender").dropdown();

    // When state dropdown changes, update lgas
    $(".states")
        .dropdown({
            onChange: function (value, text) {
                $(".lgas").dropdown("clear");
                $(".lgas .item").each(function () {
                    if ($(this).data("state") != value)
                        $(this).hide();
                    else
                        $(this).show();

                });
            }
        });

    $(".checkbox").checkbox();
}

function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};