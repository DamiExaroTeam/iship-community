﻿$(document)
    .ready(function () {
        // section selection
        $(document).on("click", ".sectionheader:not(.inactive)", function () {
            var selector = $(this).find(".chevron.circle");
            if (selector.hasClass("selected")) {
                $(this).removeClass("selected");
                selector.removeClass("selected");
                $(this).next(".item").slideUp();

            } else {
                $(this).addClass("selected");
                selector.addClass("selected");
                $(this).next(".item").slideDown();
            }

        });
        
        // order
        $(document).on("click", ".orderby", function () {
            var orderby = $(this).data("orderby");
            var perpage = $("#NumberPerPage").val();
            var reverse;
            if ($(this).hasClass("arrowdown"))
                reverse = 1;
            else
                reverse = 0;

            var url = $(".filterform").attr("action");
            UpdatePage(orderby, perpage, reverse, url);

            return false;
        });

        // print
        $(document).on("click", ".printbutton", function () {
            var printpage = 0;
            if ($(this).hasClass("printpage"))
                printpage = 1;
            var url = $(".filterform").attr("action");
            var orderby, reverse;
            if ($(".orderby.arrowdown").length) {
                orderby = $(".orderby.arrowdown").data("orderby");
                reverse = 0;
            }
            if ($(".orderby.arrowup").length) {
                orderby = $(".orderby.arrowup").data("orderby");
                reverse = 1;
            }
            var page = $("#PageNumber").val() - 1;
            var isprint = 1;


            $("#menuloader").addClass("active");

            $.ajax({
                type: "POST",
                url: url,
                data: { orderby, reverse, page, isprint, printpage },
                dataType: "json",
                error: function () {
                    $("#menuloader").removeClass("active");
                },
                success: function (result) {
                    $("#menuloader").removeClass("active");

                    $(".totalrows").text(result.Title);

                    // populate print window and show
                    $("#printwindow").html(result.page);
                    window.print();
                }
            });
        });


        // paging
        $(document).on("click", ".pagination a", function () {
            var orderby = $("#orderby").val();
            var perpage = $("#NumberPerPage").val();
            var reverse;
            if ($(".orderby").hasClass("arrowdown")) {
                reverse = 1;
            } else {
                reverse = 0;
            }
            var url = $(this).attr("href");

            UpdatePage(orderby, perpage, reverse, url);
            return false;
        });

        // on menu item clicked
        $(document).on("click", "a[data-option]", function () {
            $("#sideMenuSlide .item.active").removeClass("active");
            $(this).addClass("active");

            var controller = $(this).data("section");
            var action = $(this).data("option");
            var url = "/" + controller + "/" + action;
            $("#menuloader").addClass("active");

            // if its the dashboard close all other options
            if (controller == "Dashboard") {
                $(".sectionheader.selected").click();
            } else {
                // update sidemenu
                if (!$("#" + controller + "section").hasClass("selected"))
                    $("#" + controller + "section").click();

                var section = $("#" + $(this).data("sectiontitle"));
                if (!section.hasClass("active"))
                    section.click();

                var ss = section.next(".content").find("a[data-option='" + action + "']");
                if (!ss.hasClass("active"))
                    ss.addClass("active");
            }

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                error: function () {
                    $(".loader").removeClass("active");
                },
                success: function (result) {
                    $(".loader").removeClass("active");
                    $("#workspace").fadeOut(function() {
                        $(this).html(result.page);

                        Community();

                        ManageMyAccount();

                        OnUpdate();

                        Dashboard();

                        ActivateSearch();

                        $("#workspace").fadeIn();
                    });
                    
                    // update url
                    var loc = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "");
                    history.replaceState("", "PassFix", loc + "/Tools?section=" + controller + "&option=" + action);
                }
            });
        });
        
        // if page is refreshed
        var controller = getURLParameter("section");
        var action = getURLParameter("option");
        var subsection = getURLParameter("subsection");
        var url = "/" + controller + "/" + action;

        var item = "";
        if (action)
            item = $('a.sidemenuitem[data-option="' + action.split("?")[0] + '"][data-section="' + controller + '"]');

        if (subsection)
            controller = subsection;

        if (controller && action) {
            // close all opened accordions
            $(".accordion .title").removeClass("active");

            // activate the correct menu options
            $("#" + controller).click();

            // if there are parameters, execute string instead
            if (action.split("?").length != 1) {
                // remove dashboard menu active
                $(".item.title.dashboard").removeClass("active");

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    error: function() {
                        $(".loader").removeClass("active");
                    },
                    success: function(result) {
                        $(".loader").removeClass("active");
                        $("#workspace").fadeOut(function() {
                            $(this).html(result.page);

                            OnUpdate();

                            $(".dropdown").dropdown();

                            ActivateSearch();

                            $(this).fadeIn();
                        });
                    }
                });
            }
            else {
                item.click();
            } 
        }
        else
        {
            Dashboard();
        }

        // turn off autocomplete
        $(document).on("focus", ":input", function () {
            $(this).attr("autocomplete", "off");
        });

        // search dashboard
        $(document).on("click", ".searchbtn", function() {
            $(".searchdiv").slideToggle();
        });
        
        // Privileges
        $(document).on("click", ".modalaction", function () {
            var btn = $(this);
            var id = btn.data("id");
            var url = btn.data("url");
            var form = btn.closest("form");
            var modal = $("#editAccessModal");

            $("#manageUserid").val(id);
            form.action = url;

            // loading
            btn.addClass("loading");

            $.ajax({
                type: "POST",
                url: url,
                data: { id: id },
                dataType: "json",
                statusCode: {
                    403: function () {
                        showError(null, null, msgType.LoggedOut);
                        window.location.href = "/";
                    }
                },
                error: function () {
                    showError(null, null, msgType.Error);
                    btn.removeClass("loading");
                },
                success: function (result) {
                    btn.removeClass("loading");
                    modal.html(result.page);

                    $("body .privileges.modals").remove();
                    modal.modal({
                                transition: "horizontal flip",
                                onVisible: function () {
                                    var container = $(this).find(".container");
                                    container.css("min-height", container.height() + "px");
                                }
                            })
                            .modal("show")
                        ;
                }
            });
        });

        // show starter menu
        //$(".startersector").click();
        //$(".startersector").next(".accordionwrap").slideDown();
    });

function getURLParameter(name) {
    return decodeURIComponent(
        (new RegExp("[?|&]" + name + "=" + "([^&;]+?)(&|#|;|$)").exec(location.search) || [null, ""])[1].replace(/\+/g, "%20")) || null;
}

function OnUpdate() {
    // Search and update
    $(".searchform")
        .form({
            className: { success: "" },
            onSuccess: function (event) {
                $(this).find(".submit").addClass("loading");
            }
        })
        .api({
            serializeForm: true,
            method: "POST",
            onSuccess: function (response) {
                if (response.error) {
                    var errors = [response.error];
                    $(this).form("add errors", errors);
                }
                else {
                    // enable update button
                    $("#searchresults").find(".submit").removeClass("disabled");

                    // clear any previous error
                    $(this).find(".error .header").text();

                    // remove loading
                    $(this).find(".submit").removeClass("loading");

                    // update page
                    $("#searchresults").html(response.page);

                    // enable anything that requires it
                    if (response.enable) {
                        $(".ui.disabled").removeClass("disabled");
                    }

                    // active search if required
                    if (response.activesearch)
                        ActivateSearch();

                    // allow update now
                    OnUpdate();}
                
            },
            onFailure: function (response) {
                // remove loading
                $(this).find(".submit").removeClass("loading");

                if (response.error) {
                    var errors = [response.error];
                    $(this).form("add errors", errors);
                } else {
                    $(this).find(".message").hide();
                }
            }
        });

    $(".update.form")
        .form({
            className: { success: "" },
            fields: {
                emailaddress: {
                    identifier: "email",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please enter your e-mail"
                        },
                        {
                            type: "regExp[/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/]",
                            prompt: "Please enter a valid email address"
                        }
                    ]
                },
                firstname: {
                    identifier: "firstname",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please enter your firstname"
                        },
                        {
                            type: "maxLength[20]",
                            prompt: "Your firstname must be at most 20 characters"
                        }
                    ]
                },
                surname: {
                    identifier: "surname",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please enter your surname"
                        },
                        {
                            type: "maxLength[20]",
                            prompt: "Your surname must be at most 20 characters"
                        }
                    ]
                }
            }
        })
        .api({
            serializeForm: true,
            method: "POST",
            onSuccess: function (response) {
                var form = $(this);

                // show success message
                form.addClass("success");

                // stop user from posting again
                $("#updateform").find(".submit").addClass("disabled");

                // clear any previous error
                form.find(".field.error").removeClass("error");
                form.find(".ui.error.message ul").remove();

                // after a few seconds, clear form
                setTimeout(
                    function () {
                        if (response.clearform) {
                            ClearForm();
                        }

                        if (response.page) {
                            $("#searchresults").fadeOut(function () {
                                $(this).html(response.page);
                                if (response.hideonupdate) {
                                    $(".hideonupdate").hide();
                                }
                                $(this).fadeIn();
                            });
                        }

                        if (!response.hasOwnProperty("keepsuccess"))
                            form.removeClass("success");

                        if (response.close) {
                            $(".deny.button").click();
                        }
                    },
                    3000);
            },
            onFailure: function (response) {
                if (response.success != 0) {
                    showError(null, null, msgType.Error);
                } else {
                    if (response.error) {
                        if (response.hideform)
                            $("#updateform").fadeOut();

                        var errors = [response.error];
                        $(this).form("add errors", errors);
                    }
                }
            }
        });
    }

function ClearForm() {
    $(".update.form").form("clear");
    $(".clearable").text("");
}

function ActivateSearch() {
    $("#rangestart").calendar({
        type: "date",
        formatter: {
            date: function (date) {
                if (!date) return "";
                var day = date.getDate() + "";
                if (day.length < 2) {
                    day = "0" + day;
                }
                var month = (date.getMonth() + 1) + "";
                if (month.length < 2) {
                    month = "0" + month;
                }
                var year = date.getFullYear();
                return day + "/" + month + "/" + year;
            }
        },
        endCalendar: $("#rangeend"),
        onHide: function () {
            $("#SearchStartDate").blur();
        }
    });

    $("#rangeend").calendar({
        type: "date",
        formatter: {
            date: function (date) {
                if (!date) return "";
                var day = date.getDate() + "";
                if (day.length < 2) {
                    day = "0" + day;
                }
                var month = (date.getMonth() + 1) + "";
                if (month.length < 2) {
                    month = "0" + month;
                }
                var year = date.getFullYear();
                return day + "/" + month + "/" + year;
            }
        },
        startCalendar: $("#rangestart"),
        onHide: function () {
            $("#SearchEndDate").blur();
        }
    });

    $(".communitylist, .genderlist").dropdown({
        forceSelection: false
    });

    $(".ui.searchaudit")
        .checkbox().checkbox({
            onChecked: function () {
                $(".auditsearch").slideDown();
            },
            onUnchecked: function () {
                $(".auditsearch").slideUp();
            }
        });

    $(".clearsearch").click(function () {
        $(this).closest("form").form("clear");
    });

    $(".filterform").submit(function (e) {
        e.preventDefault();
        $("#perpage").val($("#NumberPerPage").val());
        if ($(".orderby.arrowdown").length) {
            $("#reverse").val(1);
            $("#orderby").val($(".orderby.arrowdown").data("orderby"));
        }
        if ($(".orderby.arrowup").length) {
            $("#reverse").val(0);
            $("#orderby").val($(".orderby.arrowup").data("orderby"));
        }

        $(this).find(".submit").addClass("loading");
        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            dataType: "json",
            data: $(this).serialize(),
            statusCode: {
                403: function () {
                    showError(null, null, msgType.LoggedOut);
                    window.location.href = "/";
                }
            },
            error: function () {
                $(".loading").removeClass("loading");
                showError(null, null, msgType.Error);
            },
            success: function (result) {
                $(".loading").removeClass("loading");
                $("#auditSearchResults").fadeOut(function () {
                    $("tfoot").remove();
                    $(this).html(result.page);

                    $(".totalrows").text(result.total + " Records");
                    $("#NumberPerPage").dropdown();

                    $(this).fadeIn();
                    $("html, body").animate({ scrollTop: $(".searchable").position().top }, 1000);
                });
            }
        });

        $(".numberperpage").dropdown({
            onChange: function (value) {
                var orderby = $("#OrderBy").val();
                var type = $(this).data("type");
                var perpage = value;
                var reverse;

                if ($(".orderby").hasClass("arrowdown")) {
                    reverse = 1;
                } else {
                    reverse = 0;
                }

                var link = type == 2 ? "GetErrorLogsPage?page=" : "GetAuditTrailPage?page=";
                var url = "/UserManagement/" + link + $("#CurrentPage").val();

                UpdatePage(orderby, perpage, reverse, url);
            }
        });

        return false;
    });

    $(".searchbutton").click(function () {
        $(this).addClass("loading").closest("form").submit();
    });
    
    // Search table
    var $rows = $("table.searchable tbody tr");
    $("input.roundsearchbox").keyup(debounce(function () {
        var val = "^(?=.*\\b" + $.trim($(this).val()).split(/\s+/).join("\\b)(?=.*\\b") + ").*$",
            reg = RegExp(val, "i"),
            text;

        $rows.fadeIn().filter(function () {
            text = $(this).text().replace(/\s+/g, " ");
            var result = !reg.test(text);

            if (result == false)
                if ($("tbody tr:visible").length != 0)
                    $(".noneleft").hide();

            return result;
        }).fadeOut(function () {
            if ($("tbody tr:visible").length == 0)
                $(".noneleft").fadeIn();
        });
    }, 300));

}

function UpdatePage(orderby, perpage, reverse, url) {
    $("#menuloader").addClass("active");
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: { orderby, perpage, reverse },
        statusCode: {
            403: function () {
                showError(null, null, msgType.LoggedOut);
                window.location.href = "/";
            }
        },
        error: function () {
            showError(null, null, msgType.Error);
            $("#menuloader").removeClass("active");
        },
        success: function (result) {
            $("#menuloader").removeClass("active");
            $("#auditSearchResults").fadeOut(function () {
                $("tfoot").remove();
                $(this).html(result.page);

                $(".totalrows").text(result.total + " Records");
                $(this).fadeIn();
                $("body").animate({
                    scrollTop: 0
                }, 1000);
            });
        }
    });
}

function submitForm(name) {
    OnUpdate();

    $("#" + name).find(".submit").click();
}

function ShowModal(url, size, callback) {
    var modalname = size == 1 ? "dashboardModalSmall" : "dashboardModal";
    var modal = $("#" + modalname);

    $("#menuloader").addClass("active");
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        statusCode: {
            403: function () {
                showError(null, null, msgType.LoggedOut);
                window.location.href = "/";
            }
        },
        error: function () {
            showError(null, null, msgType.Error);
            $("#menuloader").removeClass("active");
        },
        success: function (result) {
            $("#menuloader").removeClass("active");
            modal.html(result.page);
            $("body .modals").remove();
            modal.modal({
                transition: "scale",
                onShow: function() {
                    callback();
                },
                onVisible: function () {
                    var container = $(this).find(".container");
                    container.css("min-height", container.height() + "px");
                }
            })
            .modal("show");
        }
    });
}