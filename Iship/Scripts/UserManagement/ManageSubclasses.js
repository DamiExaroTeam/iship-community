﻿var selectedAddlist = [];
var selectedRemovelist = [];

$(document)
    .ready(function () {
        $(document).on("click", ".selectclass .item", function () {
            $(".selectclass .item").removeClass("active");
            $(this).addClass("active");
            var id = $(this).data("val");
            $("#menuloader").addClass("active");

            $.ajax({
                type: "POST",
                url: "/usermanagement/GetSubclass",
                data: { id: id },
                dataType: "json",
                statusCode: {
                    403: function () {
                        $(".ui.modal").modal("hide");
                        showError(null, null, msgType.LoggedOut);
                        window.location.href = "/";
                    }
                },
                error: function () {
                    $("#menuloader").removeClass("active");
                    $(".ui.modal").modal("hide");
                    showError(null, null, msgType.Error);
                },
                success: function (result) {
                    $("#menuloader").removeClass("active");
                    $(".updatecats").fadeOut(function () {
                        $(this).html(result.page).fadeIn();
                    });
                }
            });
        });

        // Add permissions
        $(document).on("click",
            "#addSubclassActionBtn",
            function () {
                AddRemoveSubclassAction($(this), "AddClaimsToSubclass", selectedAddlist.join(", "));
                selectedAddlist = [];
            });

        // Remove permissions
        $(document).on("click",
            "#removeSubclassActionBtn",
            function () {
                AddRemoveSubclassAction($(this), "RemoveClaimsFromSubclass", selectedRemovelist.join(", "));
                selectedRemovelist = [];
            });
    });

function AddRemoveSubclassAction(btn, urlaction, claims) {
    btn.addClass("loading");
    var id = btn.data("id");

    $.ajax({
        type: "POST",
        url: "/usermanagement/" + urlaction,
        data: { id: id, claims: claims },
        dataType: "json",
        statusCode: {
            403: function () {
                $(".ui.modal").modal("hide");
                showError(null, null, msgType.LoggedOut);
                window.location.href = "/";
            }
        },
        error: function () {
            $(".ui.modal").modal("hide");
            showError(null, null, msgType.Error);
        },
        success: function (result) {
            btn.removeClass("loading");
            $(".updatecats").fadeOut(function () {
                $(this).html(result.page).fadeIn();
            });

            $("#sideMenu").fadeOut(function () {
                $(this).html(result.menu).fadeIn();
                // initialize accordions
                $(".ui.accordion")
                    .accordion()
                    ;
            });
        }
    });
}
