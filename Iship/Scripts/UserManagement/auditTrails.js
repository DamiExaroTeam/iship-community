﻿function AuditTrails() {
    $("#rangestart").calendar({
        type: "date",
        endCalendar: $("#rangeend"),
        onHide: function () {
            $("#SearchStartDate").blur();
        }
    });

    $("#rangeend").calendar({
        type: "date",
        startCalendar: $("#rangestart"),
        onHide: function () {
            $("#SearchEndDate").blur();
        }
    });

    $(".numberperpage").dropdown({
        onChange: function (value) {
            var orderby = $("#OrderBy").val();
            var type = $(this).data("type");
            var perpage = value;
            var reverse;

            if ($(".orderby").hasClass("arrowdown")) {
                reverse = 1;
            } else {
                reverse = 0;
            }

            var link = type == 2 ? "GetErrorLogsPage?page=" : "GetAuditTrailPage?page=";
            var url = "/UserManagement/" + link + $("#CurrentPage").val();

            UpdatePage($("#CurrentPage").val(), orderby, perpage, reverse, url);
        }
    });

    $(".ui.searchaudit")
        .checkbox().checkbox({
            onChecked: function () {
                $(".auditsearch").slideDown();
            },
            onUnchecked: function () {
                $(".auditsearch").slideUp();
            }
        });

    $("#ClearAudit").click(function () {
        $(this).closest("form").form("clear");
    });

    $("#SearchAudit").click(function () {
        var btn = $(this);
        btn.addClass("loading");
        var perpage = $("#NumberPerPage").val();
        var action = $("#SearchAction").val();
        var userid = $("#SearchUser").val();
        var start = $("#SearchStartDate").val();
        var end = $("#SearchEndDate").val();
        var formno = $("#FormNo").val();
        var docno = $("#DocNo").val();
        var url = "/UserManagement/SearchAudit";

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: { perpage: perpage, userid: userid, start: start, end: end, action: action, formno: formno, docno: docno },
            statusCode: {
                403: function () {
                    showError(null, null, msgType.LoggedOut);
                    window.location.href = "/";
                }
            },
            error: function () {
                showError(null, null, msgType.Error);
                btn.removeClass("loading");
            },
            success: function (result) {
                btn.removeClass("loading");
                $("#auditSearchResults").fadeOut(function () {
                    $("tfoot").remove();
                    $(this).html(result.page);

                    $(this).fadeIn(function () { });
                    $("html, body").animate({ scrollTop: $(".highertable").position().top }, 1000);
                });
            }
        });
    });

    SetEvents();
}

function GetInnerException(id) {
    $("#menuloader").addClass("active");
    $.ajax({
        type: "POST",
        url: "/UserManagement/GetInnerException",
        dataType: "json",
        data: { id },
        statusCode: {
            403: function () {
                showError(null, null, msgType.LoggedOut);
                window.location.href = "/";
            }
        },
        error: function () {
            showError(null, null, msgType.Error);
            $("#menuloader").removeClass("active");
        },
        success: function (result) {
            $("#menuloader").removeClass("active");
            showError("Inner Exception", result.msg, 3);
        }
    });
}

function UpdatePage(orderby, perpage, reverse, url) {
    $("#menuloader").addClass("active");
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: { orderby, perpage, reverse },
        statusCode: {
            403: function () {
                showError(null, null, msgType.LoggedOut);
                window.location.href = "/";
            }
        },
        error: function () {
            showError(null, null, msgType.Error);
            $("#menuloader").removeClass("active");
        },
        success: function (result) {
            $("#menuloader").removeClass("active");
            $("#auditSearchResults").fadeOut(function () {
                $("tfoot").remove();
                $(this).html(result.page);

                $(this).fadeIn();
                $("body").animate({
                    scrollTop: 0
                }, 1000);
            });
        }
    });
}

function SetEvents() {
    $(".pagination a").click(
        function () {
            var orderby = $("#OrderBy").val();
            var perpage = $("#NumberPerPage").val();
            var reverse;
            if ($(".orderby").hasClass("arrowdown")) {
                reverse = 1;
            } else {
                reverse = 0;
            }
            var url = $(this).attr("href");

            UpdatePage(orderby, perpage, reverse, url);
            return false;
        });
}