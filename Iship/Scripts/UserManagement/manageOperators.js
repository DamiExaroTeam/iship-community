﻿var selectedAddlist = [];
var selectedRemovelist = [];

$(document)
    .ready(function () {
        // Initialize checkbox
        $("#manageDisabledUsers").checkbox({
            onChecked: function () {
                $(".disabledusers").slideDown();


            },
            onUnchecked: function () {
                $(".disabledusers").slideUp();
            }
        });

        // Action buttons
        $(document).on("click", ".operatorstatus", function () {
            var btn = $(this);
            var id = btn.data("id");
            var url = btn.data("url");
            var lock = btn.data("lockout");

            if (lock == 1)
                $("#minimodalcontent").text("Are you sure you want to disable this user?");
            else
                $("#minimodalcontent").text("Are you sure you want to enable this user?");

            $(".ui.mini.modal")
                    .modal({
                        closable: false,
                        onApprove: function () {
                            $(".positive.button").addClass("loading");
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: { id: id, status: lock },
                                dataType: "json",
                                error: function () {
                                    $(".positive.button").removeClass("loading");
                                },
                                success: function (result) {
                                    $(".positive.button").removeClass("loading");
                                    if (result.success == 1) {
                                        $("#manageOperators").fadeOut(function () {
                                            $(this).html(result.page).fadeIn();
                                            ShowDisabledUsers();
                                        });
                                        $(".negative.button").click();
                                    } else
                                        $(this).find(".error.message").fadeIn();
                                }
                            });

                            return false;
                        }
                    })
                    .modal("show")
                ;
        });

        // search menu
        $(document).on("input", ".searchinput", function () {
            var filter = $(this).val().toUpperCase();

            // Loop through all list items, and hide those who don't match the search query
            $(this).closest(".menu").find(".selectable .item").each(function () {
                if ($(this).text().toUpperCase().indexOf(filter) > -1) {
                    $(this).fadeIn();
                } else {
                    $(this).fadeOut();
                }
            });
        });

        // Add action
        $(document).on("click", ".addaction .selectable a", function () {
            if ($(this).hasClass("optionselected")) {
                $(this).removeClass("optionselected");
                var id = $(this).data("val");
                var index = selectedAddlist.indexOf(id);
                selectedAddlist.splice(index, 1);

                if (selectedAddlist.length == 0)
                    $("#addActionBtn, #addSubclassActionBtn").addClass("disabled");
            } else {
                $(this).addClass("optionselected");
                $("#addActionBtn, #addSubclassActionBtn").removeClass("disabled");
                selectedAddlist.push($(this).data("val"));
            }
        });
        
        // Remove action
        $(document).on("click", ".removeaction .selectable a", function () {
            if ($(this).hasClass("optionselected")) {
                $(this).removeClass("optionselected");
                var id = $(this).data("val");
                var index = selectedAddlist.indexOf(id);
                selectedRemovelist.splice(index, 1);

                if (selectedRemovelist.length == 0)
                    $("#removeActionBtn, #removeSubclassActionBtn").addClass("disabled");
            } else {
                $(this).addClass("optionselected");
                $("#removeActionBtn, #removeSubclassActionBtn").removeClass("disabled");
                selectedRemovelist.push($(this).data("val"));
            }
           
        });

        // Add permissions
        $(document).on("click",
            "#addActionBtn",
            function() {
                AddRemoveAction($(this), "addClaims", selectedAddlist.join(", "));
                selectedAddlist = [];
            });

        // Remove permissions
        $(document).on("click",
            "#removeActionBtn",
            function () {
                AddRemoveAction($(this), "removeClaims", selectedRemovelist.join(", "));
                selectedRemovelist = [];
            });

        // select all
        $(document).on("click",
            ".selectall",
            function () {
                var btn = $(this);
                $(".addaction .selectable a").each(function () {
                    if (btn.text() == "Select All") {
                        if (!$(this).hasClass("optionselected"))
                            $(this).click();
                    } else {
                        if ($(this).hasClass("optionselected"))
                            $(this).click();
                    }
                });

                if (btn.text(btn.text() == "Select All" ? "Deselect All" : "Select All"));
            });

        // deselect all
        $(document).on("click",
            ".removeall",
            function () {
                var btn = $(this);
                $(".removeaction .selectable a").each(function () {
                    if (btn.text() == "Select All") {
                        if (!$(this).hasClass("optionselected"))
                            $(this).click();
                    } else {
                        if ($(this).hasClass("optionselected"))
                            $(this).click();
                    }
                });
                btn.text(btn.text() == "Select All" ? "Deselect All" : "Select All");
            });

        ShowDisabledUsers();
    });

function AddRemoveAction(btn, urlaction, claims) {
    var modal = $("#EditClaimsModal");
    btn.addClass("loading");
    var id = btn.data("id");
    var controller = btn.data("controller");

    $.ajax({
        type: "POST",
        url: "/" + controller + "/" + urlaction,
        data: { id: id, claims: claims },
        dataType: "json",
        statusCode: {
            403: function () {
                $(".ui.modal").modal("hide");
                showError(null, null, msgType.LoggedOut);
                window.location.href = "/";
            }
        },
        error: function () {
            $(".ui.modal").modal("hide");
            showError(null, null, msgType.Error);
        },
        success: function (result) {
            btn.removeClass("loading");
            $(".ui.transparentbtn").show().delay(1000).fadeOut();
            modal.fadeOut(function () {
                $(this).html(result.page).fadeIn(function() {
                    $(window).resize();
                });
              
            });
        }
    });
}


function ShowDisabledUsers() {
    $("#manageDisabledUsers").checkbox({
        onChecked: function () {
            $(".disabledusers").slideDown();


        },
        onUnchecked: function () {
            $(".disabledusers").slideUp();
        }
    });

    $(".adminCheckbox").checkbox();
}