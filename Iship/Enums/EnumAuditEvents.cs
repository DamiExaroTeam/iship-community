﻿using System.ComponentModel;

namespace Iship.Enums
{
    public enum EnumAuditEvents
    {
        [Description("Operator logged in successfully")]
        LoginSuccess = 1,
        [Description("Operator logged out")]
        LogOut = 2,
        [Description("Operator failed to login with username and password")]
        LoginFailed = 3,
        [Description("Operator was sent a registration key")]
        OperatorKeySent = 4,
        [Description("Operator used registration key")]
        OperatorRegistered = 5,
        [Description("Assign privilege to Operator")]
        PrivilegeAssigned = 6,
        [Description("Revoke privilege from Operator")]
        PrivilegeRevoked = 7,
        [Description("Disable a Operator")]
        DisableOperator = 8,
        [Description("Operator updated personal details")]
        PersonalDetailsUpdated = 9,
        [Description("Operator changed password")]
        PasswordChanged = 10,
        [Description("Enable a Operator")]
        EnableOperator = 11,
        [Description("Assign privilege to Subclass")]
        PrivilegeAssignedSubclass = 12,
        [Description("Revoke privilege from Subclass")]
        PrivilegeRevokedSubclass = 13,
        [Description("Assign subclass to Section")]
        SubclassAssignedSection = 14,
        [Description("Revoke subclass from Section")]
        SubclassRevokedSection = 15,
        [Description("Create community")]
        AddCommunity = 16,
        [Description("Edit community")]
        EditCommunity = 17,
        [Description("Create new community operator")]
        AddCommunityOperator = 18,
        [Description("Update Operator Admin status")]
        UpdateOperatorAdminStatus = 19

    }
}
