﻿namespace Iship.Enums
{
    public enum EnumOperatorType
    {
        SystemAdministrator = 1,
        CommunityAdministrator = 2,
        CommunityUser = 3,
        SystemReadOnly = 4,
        PrimarySchoolsUser = 5,
        PrimarySchoolsAdministrator = 6
    }
}