﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Iship.Startup))]
namespace Iship
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
