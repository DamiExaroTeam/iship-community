﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Iship
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "tools",
                url: "Tools/{id}",
                defaults: new { controller = "Home", action = "Tools", id = UrlParameter.Optional });

            routes.MapRoute(
                name: "error",
                url: "Error/{id}",
                defaults: new { controller = "Home", action = "Error", id = UrlParameter.Optional });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
