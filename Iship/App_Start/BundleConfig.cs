﻿using System.Web.Optimization;

namespace Iship
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));


            // Layout page
            bundles.Add(new ScriptBundle("~/bundles/layout").Include(
                "~/scripts/lib/jquery/dist/jquery.js",
                "~/scripts/jq-serialize.js",
                "~/scripts/semantic.js",
                "~/scripts/site.js"));

            bundles.Add(new StyleBundle("~/Content/layout").Include(
                "~/content/semantic/semantic.css",
                "~/content/fontawesome-all.css",
                "~/content/site.css"));

            // Home page
            bundles.Add(new ScriptBundle("~/bundles/home").Include(
                "~/scripts/home.js",
                "~/scripts/calendar.min.js"
                ));

            bundles.Add(new StyleBundle("~/Content/home").Include(
                "~/content/home.css",
                "~/content/calendar.css"
                ));

            // Main
            bundles.Add(new ScriptBundle("~/bundles/tools").Include(
                "~/scripts/UserManagement/manageMyAccount.js",
                "~/scripts/calendar.min.js",
                "~/scripts/UserManagement/manageOperators.js",
                "~/scripts/chartist.min.js",
                "~/scripts/chartist-plugin-axistitle.js", 
                "~/scripts/community.js",
                "~/scripts/dashboard.js",
                "~/scripts/tools.js"
            ));

            bundles.Add(new StyleBundle("~/Content/tools").Include(
                "~/content/tools.css",
                "~/content/dashboard.css",
                "~/content/calendar.css"
            ));


            BundleTable.EnableOptimizations = false;
        }
    }
}
