﻿using System.Security.Claims;
using System.Web.Mvc;

namespace Iship.Helpers
{
    public class ClaimRequirement : AuthorizeAttribute
    {
        private readonly string _claimType;
        private readonly string _claimValue;
        public ClaimRequirement(string type, string value)
        {
            _claimType = type;
            _claimValue = value;
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User is ClaimsPrincipal user && user.HasClaim(_claimType, _claimValue))
            {
                base.OnAuthorization(filterContext);
            }
            else
            {
                HandleUnauthorizedRequest(filterContext);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Response.StatusCode = 412;
                filterContext.Result = new HttpStatusCodeResult(412);
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }

}

