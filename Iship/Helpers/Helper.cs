﻿using Iship.Enums;
using Iship.Extensions;
using Iship.Models;
using Iship.Services;
using Iship.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Iship.Helpers
{
    public static class Helper
    {
        private static readonly Random Random = new Random();

        public static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        public static string SplitCamelCase(this string str)
        {
            return Regex.Replace(
                Regex.Replace(
                    str,
                    @"(\P{Ll})(\P{Ll}\p{Ll})",
                    "$1 $2"
                ),
                @"(\p{Ll})(\P{Ll})",
                "$1 $2"
            );
        }

        public static string DecryptConfig(string encrypted, bool isConnString = true)
        {
            var fluff = ConfigurationManager.AppSettings["salt"];

            if (!isConnString)
                return encrypted.Replace(fluff.ToArray(), "");

            var builder = new OracleConnectionStringBuilder(encrypted);
            var password = builder.Password;

            var plain = password.Replace(fluff.ToArray(), "");
            builder.Password = plain;

            return builder.ConnectionString;
        }

        public static T DeepCopy<T>(T other)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, other);
                ms.Position = 0;
                return (T)formatter.Deserialize(ms);
            }
        }

        public static string JsonPartialView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (var stringWriter = new StringWriter())
            {
                var partialView = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, partialView.View, controller.ViewData, controller.TempData, stringWriter);
                partialView.View.Render(viewContext, stringWriter);
                partialView.ViewEngine.ReleaseView(controller.ControllerContext, partialView.View);
                return stringWriter.ToString();
            }
        }

        public static EditUserClaimsWrap GetUserClaims(int adminid, int oprid)
        {
            var auth = new AuthService();
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var claims = new List<EditUserClaims>();
            var admin = userManager.FindById(adminid);
            var user = userManager.FindById(oprid);
            var userclaims = userManager.GetClaims(user.Id);
            var allclaims = auth.GetOperatorClaimsList();

            // remove any claims which the admin shouldn't have access too
            var removeme = new List<UserClaim>();
            foreach (var claim in allclaims)
            {
                if (admin.OperatorType == (int)EnumOperatorType.SystemAdministrator)
                {
                    switch (user.OperatorType)
                    {
                        case (int)EnumOperatorType.SystemAdministrator:
                            continue;
                        case (int)EnumOperatorType.SystemReadOnly:
                            // view all
                            if (!claim.Action.Contains("View") && claim.Action != "MyAccount")
                                removeme.Add(claim);
                            break;
                        case (int)EnumOperatorType.CommunityAdministrator:
                            // view and edit all community
                            if (claim.Controller != "Community" && claim.Action != "MyAccount")
                                removeme.Add(claim);
                            break;
                        case (int)EnumOperatorType.CommunityUser:
                            // Anything that is in usermanagement section
                            if (claim.Controller != "Community" && claim.Action != "MyAccount" || claim.SubSectionId == 3)
                                removeme.Add(claim);
                            break;
                    }
                }

                if (admin.OperatorType == (int)EnumOperatorType.CommunityAdministrator)
                {
                    switch (user.OperatorType)
                    {
                        case (int)EnumOperatorType.CommunityAdministrator:
                            // view and edit all community
                            if (claim.Controller != "Community" && claim.Action != "MyAccount")
                                removeme.Add(claim);
                            break;
                        case (int)EnumOperatorType.CommunityUser:
                            // Anything that is in usermanagement section
                            if (claim.Controller != "Community" && claim.Action != "MyAccount" || claim.SubSectionId == 3)
                                removeme.Add(claim);
                            break;
                    }
                }
            }

            // remove these claims
            allclaims = allclaims.Except(removeme).ToList();

            // selected
            foreach (var claim in userclaims)
            {
                var userclaim = claims.FirstOrDefault(x => x.Controller == claim.Type);

                if (userclaim == null)
                {
                    userclaim = new EditUserClaims
                    {
                        Controller = claim.Type
                    };

                    claims.Add(userclaim);
                }

                userclaim.SelectedClaims.Add(claim.Value);
            }

            // unselected
            foreach (var claim in allclaims)
            {
                var userclaim = claims.FirstOrDefault(x => x.Controller == claim.Controller);
                if (userclaim == null)
                {
                    userclaim = new EditUserClaims
                    {
                        Controller = claim.Controller
                    };

                    claims.Add(userclaim);
                }

                if (userclaim.Controller == claim.Controller && !userclaim.SelectedClaims.Contains(claim.Action))
                    userclaim.UnSelectedClaims.Add(claim.Action);
            }

            // order them
            foreach (var c in claims)
            {
                c.SelectedClaims = c.SelectedClaims.OrderBy(x => x).ToList();
                c.UnSelectedClaims = c.UnSelectedClaims.OrderBy(x => x).ToList();
            }

            return new EditUserClaimsWrap
            {
                UserId = oprid.ToString(),
                Claims = claims
            };
        }
    }
}
