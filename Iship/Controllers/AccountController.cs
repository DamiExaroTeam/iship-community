﻿using Iship.Extensions;
using Iship.Helpers;
using Iship.Models;
using Iship.Models.Account;
using Iship.Services;
using Iship.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Iship.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : BaseController
    {
        private readonly AuthService _auth = new AuthService();

        public ApplicationSignInManager SignInManager => HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
        public ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public string ErrorMessage { get; set; }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var signedUser = await UserManager.FindByEmailAsync(model.Email);

                if (signedUser == null)
                    return Json(new
                    {
                        success = 0,
                        error = "Invalid username or password"
                    });

                var result = await SignInManager.PasswordSignInAsync(signedUser.UserName, model.Password, model.RememberMe, shouldLockout: false);

                // Check if its first time user
                if (signedUser.IsFirstTimeLogin)
                {
                    if (result != SignInStatus.Success)
                    {
                        _auth.UserLoginFailed(signedUser.Id);
                        return Json(new
                        {
                            success = 0,
                            error = "Invalid username or password"
                        });
                    }

                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                    return Json(new
                    {
                        success = 1,
                        isfirsttime = 1,
                        userid = signedUser.Id
                    });
                }

                if (result == SignInStatus.Success)
                {
                    return LoginUser(signedUser);
                }

                if (result == SignInStatus.LockedOut)
                {
                    return Json(new
                    {
                        success = 0,
                        error = "You account has been locked",
                        returnUrl
                    });
                }

                _auth.UserLoginFailed(signedUser.Id);
                return Json(new
                {
                    success = 0,
                    error = "Invalid username or password"
                });
            }

            return Json(new
            {
                success = 0,
                errors = string.Join(" ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
            });
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            _auth.UserLoggedOut(int.Parse(User.Identity.GetUserId()));

            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                //var user = await UserManager.FindByEmailAsync(model.Email);
                var user = _auth.GetUserByEmail(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return Json(new
                    {
                        success = 0,
                        error = "Sorry, no registered user with that email address"
                    });
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id.ToString(), code, Request.Url?.Scheme);

                var iris = ConfigurationManager.AppSettings["companyEmail"];
                var friendlyName = ConfigurationManager.AppSettings["friendlyName"];
                var emailcredentials = ConfigurationManager.AppSettings["email"];
                var emailpassword = Helper.DecryptConfig(ConfigurationManager.AppSettings["password"], false);
                var styledBtn = "style='font-size: 16px;background-color: #027bbb;color: white;text-decoration: none;padding: 7px 20px;margin: 7px 0px;display: " +
                                "inline-block;-webkit-box-shadow: 2px 2px 5px rgba(0,0,0,0.3);-moz-box-shadow: 2px 2px 5px rgba(0,0,0,0.3);" +
                                "box-shadow: 2px 2px 5px rgba(0,0,0,0.3);border-radius: 5px;'";
                string subject = "Iship password reset";
                string message = $@"<div>Hello { user.FirstName.ToTitleCase() } { user.Surname.ToTitleCase() },</div>
                                    <div style='margin-top: 10px'>A request was made to reset your password on the Iship application.</div>
                                    <div style='margin-top: 10px'>Please click on the button below to reset your password:</div>
                                    <div style='margin-top: 10px'>
                                        <a href='{ callbackUrl }' {styledBtn}>Reset My Password</a>
                                    <div style='margin-top: 10px'>If you did not make this request please contact an administrator.</div>                                                                
                                    <div style='margin-top: 10px'>Thank you</div>";

                var mailMessage = new MailMessage
                {
                    From = new MailAddress(iris, friendlyName),
                    IsBodyHtml = true
                };
                mailMessage.To.Add(user.Email);
                mailMessage.Body = message;
                mailMessage.Subject = subject;

                using (var client = new SmtpClient("smtp.gmail.com", 587)
                {
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(emailcredentials, emailpassword)
                })
                {
                    client.Send(mailMessage);

                    return Json(new
                    {
                        success = 1
                    });
                }
            }

            return Json(new
            {
                success = 0,
                error = "Invalid email address"
            });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (string.IsNullOrEmpty(model.OldPassword) || string.IsNullOrEmpty(model.NewPassword))
                return Json(new
                {
                    success = 0,
                    error = "You must enter the password sent to your email and also create a new one"
                });

            if (model.ConfirmPassword != model.NewPassword)
                return Json(new
                {
                    success = 0,
                    error = "Password and Confirm password do not match"
                });

            var result = await UserManager.ChangePasswordAsync(int.Parse(model.FirstTimeId), model.OldPassword, model.NewPassword);

            if (result.Succeeded)
            {
                // change first time status
                _auth.UpdateFirstTime(model.FirstTimeId);
                var signedUser = await UserManager.FindByIdAsync(Int32.Parse(model.FirstTimeId));

                return LoginUser(signedUser);
            }

            return Json(new
            {
                success = 0,
                error = result.Errors.First()
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new
                {
                    success = 0,
                    errors = ModelState.ToDictionary(
                        kvp => kvp.Key,
                        kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                    )
                });
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "Invalid credentials"
                });
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                // change first time status
                _auth.UpdateFirstTime(user.Id.ToString());

                return Json(new
                {
                    success = 1
                });
            }

            return Json(new
            {
                success = 0
            });
        }

        private ActionResult LoginUser(ApplicationUser signedUser)
        {
            // audit
            _auth.UserLoggedIn(signedUser.Id);

            // Get user claims
            var auth = new AuthService();

            //var dashboard = new DataService().GetDashboardData(signedUser.OperatorType);
            var claims = auth.GetUserClaims(signedUser.Id);
            var sections = auth.GetSections();

            // assign claims to sections
            foreach (var section in sections)
            {
                foreach (var subsection in section.SubSections)
                {
                    subsection.Claims.AddRange(claims.Where(x => x.Controller == section.Name && x.SubSectionId == subsection.Id));
                }
            }

            var menuModel = new MenuModel
            {
                Username = signedUser.UserName,
                Sections = sections,
                Claims = claims.OrderBy(x => x.Action).ToList(),
                //Dashboard = dashboard
            };

            return Json(new
            {
                success = 1,
                homepage = Helper.JsonPartialView(this, "~/Views/Home/Tools.cshtml", menuModel),
                menu = Helper.JsonPartialView(this, "~/Views/Shared/_SideMenu.cshtml", menuModel)
            });
        }
    }
}