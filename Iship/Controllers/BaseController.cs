﻿using Iship.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;

namespace Iship.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            // for testing purposes
            var message = filterContext.Exception.Message;
            var innerex = filterContext.Exception.InnerException;

            var exception = filterContext.Exception;

            // if its antiforgery token message, just log the person out

            var code = (int)HttpStatusCode.InternalServerError; // 500 if unexpected
            var result = JsonConvert.SerializeObject(new { error = exception.Message });

            if (message.Contains("A network-related")) code = (int)HttpStatusCode.RequestTimeout;
            else if (exception is FileNotFoundException) code = (int)HttpStatusCode.NotFound;
            else if (exception is UnauthorizedAccessException) code = (int)HttpStatusCode.Unauthorized;
            else if (exception is HttpRequestException) code = (int)HttpStatusCode.BadRequest;

            var context = filterContext.HttpContext;

            try
            {
                // save to database
                string url = context.Request.Url?.ToString();
                string user = filterContext.HttpContext.User?.Identity.GetUserId();
                new AuthService().SaveError(exception, url, user);
            }
            catch
            {
                // do nothing
            }

            filterContext.ExceptionHandled = true;
            filterContext.Result = RedirectToAction("ErrorHandled", "Home", new { code });
        }
    }
}