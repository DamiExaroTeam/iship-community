﻿using Iship.Enums;
using Iship.Extensions;
using Iship.Helpers;
using Iship.Models;
using Iship.Services;
using Iship.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using X.PagedList;

namespace Iship.Controllers
{
    [Authorized]
    public class CommunityController : BaseController
    {
        private readonly DataService _data = new DataService();
        private readonly AuthService _auth = new AuthService();
        public ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        [ClaimRequirement("Community", "ViewCommunities")]
        public ActionResult ViewCommunities()
        {
            var perpage = int.Parse(ConfigurationManager.AppSettings["auditPerPage"]);
            var communitywrap = _data.GetCommunities(perpage, 0, "", 1);

            communitywrap.Page = new StaticPagedList<Community>(communitywrap.Communities, 1, perpage, communitywrap.Total);
            communitywrap.OrderBy = "Name";
            communitywrap.Order = "arrowdown";
            communitywrap.CurrentPage = 1;
            communitywrap.Title = "All Communities (" + communitywrap.Total.ToString("N0") + ")";

            // Search values
            communitywrap.States = _data.GetStates();
            communitywrap.Lgas = _data.GetLgas();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Community/_CommunitiesWrap.cshtml", communitywrap)
            });
        }

        [ClaimRequirement("Community", "ViewCommunities")]
        public ActionResult SearchCommunity(SearchParam param)
        {
            if (param.IsPrint == 0)
                param.Page = 0;

            if (string.IsNullOrEmpty(param.OrderBy))
                param.OrderBy = "Name";

            if (param.PerPage == 0)
                param.PerPage = int.Parse(ConfigurationManager.AppSettings["auditPerPage"]);

            if (Session["searchParams"] is SearchParam search && param.IsPrint == 1)
            {
                param = search;
                param.IsPrint = 1;
            }

            var communitywrap = _data.SearchCommunities(param);
            communitywrap.Page = new StaticPagedList<Community>(communitywrap.Communities, 1, param.IsPrint == 1 && param.PrintPage == 0 ? communitywrap.Total : param.PerPage, communitywrap.Total);
            communitywrap.OrderBy = param.OrderBy;
            communitywrap.Order = param.Reverse == "1" ? "arrowup" : "arrowdown";
            communitywrap.Title = communitywrap.Total.ToString("N0") + " Records";

            communitywrap.CurrentPage = 1;

            if (param.IsPrint == 1)
            {
                communitywrap.IsPrint = true;
                string title = "Communities";

                if (communitywrap.Communities.Count != 0)
                {
                    if (!string.IsNullOrEmpty(param.SearchLga))
                        title += " in " + communitywrap.Communities[0].CommunityLga;

                    if (!string.IsNullOrEmpty(param.SearchLga))
                        title += (string.IsNullOrEmpty(param.SearchLga) ? " in " : ", ") + communitywrap.Communities[0].CommunityState;

                    if (!string.IsNullOrEmpty(param.SearchStartDate))
                        title += " from " + DateTime.Parse(param.SearchStartDate).ToString("dd/MM/yyyy");

                    if (!string.IsNullOrEmpty(param.SearchEndDate))
                        title += " to " + DateTime.Parse(param.SearchEndDate).ToString("dd/MM/yyyy");
                }

                communitywrap.Title = title + $" ({communitywrap.Communities.Count:N0})";
            }
            else
                Session["searchParams"] = param;

            return Json(new
            {
                success = 1,
                total = communitywrap.Total.ToString("N0"),
                page = Helper.JsonPartialView(this, "~/Views/Community/_Communities.cshtml", communitywrap)
            });
        }

        [ClaimRequirement("Community", "ViewCommunities")]
        public ActionResult GetSearchPage(int page, string orderby, int perpage, int reverse)
        {
            CommunityWrap communityWrap;

            if (Session["searchParams"] is SearchParam searchparam)
            {
                searchparam.Page = page - 1;
                communityWrap = _data.SearchCommunities(searchparam);
                communityWrap.Page = new StaticPagedList<Community>(communityWrap.Communities, page, perpage, communityWrap.Total);
                communityWrap.OrderBy = searchparam.OrderBy;
                communityWrap.Order = reverse == 1 ? "arrowup" : "arrowdown";
                communityWrap.CurrentPage = page;
            }
            else
            {
                communityWrap = _data.GetCommunities(perpage, page - 1, orderby, 0);
                communityWrap.Page = new StaticPagedList<Community>(communityWrap.Communities, page, perpage, communityWrap.Total);
                communityWrap.OrderBy = orderby;
                communityWrap.Order = "arrowdown";
                communityWrap.CurrentPage = page;
            }

            communityWrap.Title = communityWrap.Total.ToString("N0") + " Records";

            return Json(new
            {
                success = 1,
                total = communityWrap.Total,
                page = Helper.JsonPartialView(this, "~/Views/Community/_Communities.cshtml", communityWrap)
            });
        }

        [ClaimRequirement("Community", "ViewMembers")]
        public ActionResult ViewMembers(string id, string reg)
        {
            MemberWrap memberwrap;
            var perpage = int.Parse(ConfigurationManager.AppSettings["auditPerPage"]);

            if (!string.IsNullOrEmpty(reg))
            {
                var search = new SearchParam();

                if (reg == "today")
                {
                    search.SearchStartDate = DateTime.Today.ToShortDateString();
                    search.SearchEndDate = DateTime.Today.ToShortDateString();
                    search.CustomTitle = "Community Members Registered Today";
                }

                if (reg == "yesterday")
                {
                    search.SearchStartDate = DateTime.Today.AddDays(-1).ToShortDateString();
                    search.SearchEndDate = DateTime.Today.AddDays(-1).ToShortDateString();
                    search.CustomTitle = "Community Members Registered Yesterday";
                }

                if (reg == "thisweek")
                {
                    search.SearchStartDate = DateTime.Today.StartOfWeek(DayOfWeek.Sunday).ToShortDateString();
                    search.SearchEndDate = DateTime.Today.ToShortDateString();
                    search.CustomTitle = "Community Members Registered This Week";
                }

                if (reg == "thismonth")
                {
                    search.SearchStartDate =
                        new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToShortDateString();
                    search.SearchEndDate = DateTime.Today.ToShortDateString();
                    search.CustomTitle = "Community Members Registered This Month";
                }

                memberwrap = SearchMemberOpr(search);
                memberwrap.SearchStartDate = search.SearchStartDate;
                memberwrap.SearchEndDate = search.SearchEndDate;
                memberwrap.Title = search.CustomTitle + $" ({memberwrap.Total:N0})";
            }
            else
            {
                memberwrap = _data.GetMembers(perpage, 0, "", 1, id, false);
                memberwrap.Title = "0 Records";

                if (!string.IsNullOrEmpty(id))
                {
                    memberwrap.SearchCommunity = id;
                    if (memberwrap.Members.Count != 0)
                        memberwrap.Title = $"{memberwrap.Members[0].CommunityName} ({memberwrap.Members.Count:N0})";
                }
                else
                {
                    if (memberwrap.Members.Count != 0)
                        memberwrap.Title = $"All Community Members ({memberwrap.Total:N0})";
                }
            }
           
            memberwrap.Page = new StaticPagedList<Member>(memberwrap.Members, 1, perpage, memberwrap.Total);
            memberwrap.OrderBy = "Firstname";
            memberwrap.Order = "arrowdown";
            memberwrap.CurrentPage = 1;

            // Lists
            memberwrap.States = _data.GetStates();
            memberwrap.Lgas = _data.GetLgas();
            memberwrap.Communities = _data.GetCommunities();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Community/_MembersWrap.cshtml", memberwrap)
            });
        }

        [ClaimRequirement("Community", "ViewMembers")]
        public ActionResult ViewTemporaryMembers(string id)
        {
            var perpage = int.Parse(ConfigurationManager.AppSettings["auditPerPage"]);

            var memberwrap = _data.GetMembers(perpage, 0, "", 1, id, true);
            memberwrap.Title = "0 Records";

            if (!string.IsNullOrEmpty(id))
            {
                memberwrap.SearchCommunity = id;
                if (memberwrap.Members.Count != 0)
                    memberwrap.Title = $"{memberwrap.Members[0].CommunityName} Temp Members ({memberwrap.Members.Count:N0})";
            }
            else
            {
                if (memberwrap.Members.Count != 0)
                    memberwrap.Title = $"All Temporary Community Members ({memberwrap.Total:N0})";
            }

            memberwrap.Page = new StaticPagedList<Member>(memberwrap.Members, 1, perpage, memberwrap.Total);
            memberwrap.OrderBy = "Firstname";
            memberwrap.Order = "arrowdown";
            memberwrap.CurrentPage = 1;

            // Lists
            memberwrap.States = _data.GetStates();
            memberwrap.Lgas = _data.GetLgas();
            memberwrap.Communities = _data.GetCommunities();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Community/_MembersWrap.cshtml", memberwrap)
            });
        }

        [ClaimRequirement("Community", "ViewMembers")]
        public ActionResult SearchMember(SearchParam param)
        {
            var memberwrap = SearchMemberOpr(param);

            return Json(new
            {
                success = 1,
                total = memberwrap.Total.ToString("N0"),
                page = Helper.JsonPartialView(this, "~/Views/Community/_Members.cshtml", memberwrap)
            });
        }

        [ClaimRequirement("Community", "ViewCommunities")]
        public ActionResult GetMembersSearchPage(int page, string orderby, int perpage, int reverse)
        {
            MemberWrap memberWrap;

            if (Session["searchParams"] is SearchParam searchparam)
            {
                searchparam.Page = page - 1;
                memberWrap = _data.SearchMembers(searchparam);
                memberWrap.Page = new StaticPagedList<Member>(memberWrap.Members, page, perpage, memberWrap.Total);
                memberWrap.OrderBy = searchparam.OrderBy;
                memberWrap.Order = reverse == 1 ? "arrowup" : "arrowdown";
                memberWrap.CurrentPage = page;
            }
            else
            {
                memberWrap = _data.GetMembers(perpage, page - 1, orderby, 0, "", false);
                memberWrap.Page = new StaticPagedList<Member>(memberWrap.Members, page, perpage, memberWrap.Total);
                memberWrap.OrderBy = orderby;
                memberWrap.Order = "arrowdown";
                memberWrap.CurrentPage = page;
            }

            memberWrap.Title = memberWrap.Total.ToString("N0") + " Records";

            return Json(new
            {
                success = 1,
                total = memberWrap.Total.ToString("N0"),
                page = Helper.JsonPartialView(this, "~/Views/Community/_Members.cshtml", memberWrap)
            });
        }

        [ClaimRequirement("Community", "ViewMembers")]
        public ActionResult GetCommunityMemberList(string communityId)
        {
            if (string.IsNullOrEmpty(communityId))
                return Json(new
                {
                    success = 0,
                    error = "Please select a community"
                });

            var result = _data.GetCommunity(int.Parse(communityId), true);

            return Json(new
            {
                success = 1,
                activesearch = true,
                page = Helper.JsonPartialView(this, "~/Views/Community/_MembersResult.cshtml", result)
            });
        }

        [ClaimRequirement("Community", "AddCommunity")]
        public ActionResult AddCommunity()
        {
            var community = new Community
            {
                States = _data.GetStates(),
                Lgas = _data.GetLgas()
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Community/_AddCommunity.cshtml", community)
            });
        }

        [ClaimRequirement("Community", "AddCommunity")]
        public ActionResult CreateCommunity(Community community)
        {
            var result = _data.AddCommunity(community, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Community", "EditCommunity")]
        public ActionResult ViewEditCommunity(int id)
        {
            var community = _data.GetCommunity(id, false);
            community.Lgas = _data.GetLgas();
            community.States = _data.GetStates();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Community/_EditCommunity.cshtml", community)
            });
        }

        [ClaimRequirement("Community", "EditCommunity")]
        public ActionResult EditCommunity(Community community)
        {
            var result = _data.EditCommunity(community, int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = result,
                close = true
            });
        }

        [ClaimRequirement("Community", "AddOperator")]
        public ActionResult AddOperator(ApplicationUser opr)
        {
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Community/_AddOperator.cshtml", new ApplicationUser())
            });
        }

        [ClaimRequirement("Community", "AddOperator")]
        public ActionResult CreateOperator(ApplicationUser opr)
        {
            if (ModelState.IsValid)
            {
                // if email is registered already, return message
                if (_auth.CheckEmailRegistered(opr.Email))
                {
                    return Json(new
                    {
                        success = 0,
                        error = "This email has already been registered"
                    });
                }

                if (!ModelState.IsValid)
                    return Json(new
                    {
                        success = 0,
                        error = string.Join(", ", ModelState.Values
                            .SelectMany(v => v.Errors)
                            .Select(e => e.ErrorMessage))
                    });

                string tempPassword = Helper.GenerateRandomString(5);
                int operatorid = int.Parse(User.Identity.GetUserId());
                var operatorType = opr.IsAdmin
                    ? (int) EnumOperatorType.CommunityAdministrator
                    : (int) EnumOperatorType.CommunityUser;

                var result = _auth.CreateOperator(opr, new PasswordHasher().HashPassword(tempPassword), operatorType,
                    operatorid);

                if (result == 1)
                {
                    Task.Run(() =>
                        {
                            SendRegistrationEmail(opr, tempPassword, operatorid.ToString());
                        })
                        .ContinueWith(t =>
                                new AuthService().SaveError(t.Exception, "/Community/CreateOperator",
                                    operatorid.ToString()),
                            TaskContinuationOptions.OnlyOnFaulted
                        );

                }

                return Json(new
                {
                    success = 1,
                    clearform = true
                });
            }

            return Json(new
            {
                success = 0,
                errors = string.Join(" ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
            });
        }

        [ClaimRequirement("Community", "ManageOperators")]
        public ActionResult ManageOperators()
        {
            var oprtypes = new List<int>
            {
                (int)EnumOperatorType.CommunityUser, (int)EnumOperatorType.CommunityAdministrator
            };

            var operators = _auth.GetOperators(oprtypes, int.Parse(User.Identity.GetUserId()));

            // referenced in view
            ViewBag.Section = "Community";

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Shared/_ManageOperatorsWrap.cshtml", operators)
            });
        }

        [ClaimRequirement("Community", "ManageOperators")]
        public ActionResult AddClaims(int id, string claims)
        {
            _auth.AddClaimsToUser(id, claims, int.Parse(User.Identity.GetUserId()));

            var model = Helper.GetUserClaims(int.Parse(User.Identity.GetUserId()), id);
            model.Controller = "Community";

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditUserClaimsModal.cshtml", model)
            });
        }

        [ClaimRequirement("Community", "ManageOperators")]
        public ActionResult RemoveClaims(int id, string claims)
        {
            _auth.RemoveClaimsFromUser(id, claims, int.Parse(User.Identity.GetUserId()));

            var model = Helper.GetUserClaims(int.Parse(User.Identity.GetUserId()), id);
            model.Controller = "Community";

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditUserClaimsModal.cshtml", model)
            });
        }

        [ClaimRequirement("Community", "ManageOperators")]
        public ActionResult GetUserPermissions(int id)
        {
            var claims = Helper.GetUserClaims(int.Parse(User.Identity.GetUserId()), id);
            claims.Controller = "Community";

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Shared/_EditUserClaims.cshtml", claims)
            });
        }

        [ClaimRequirement("Community", "ManageOperators")]
        public ActionResult SetAdmin(int id, int status)
        {
            var result = _auth.SetOperatorAsAdmin(id, status, int.Parse(User.Identity.GetUserId()));

            if (result == 1)
                return ManageOperators();

            return Json(new
            {
                success = result
            });
        }

        [ClaimRequirement("Community", "DuplicateResolver")]
        public ActionResult DuplicateResolver()
        {
            var duplicates = _data.GetDuplicateMembers((int)EnumMemberType.Community);

            return Json(new
            {
                success = 1,
                total = duplicates.Count.ToString("N0"),
                page = Helper.JsonPartialView(this, "~/Views/Community/_Duplicates.cshtml", duplicates)
            });
        }

        [ClaimRequirement("Community", "ManageOperators")]
        public ActionResult SetOperatorStatus(int id, int status)
        {
            var result = _auth.SetOperatorStatus(id, status, int.Parse(User.Identity.GetUserId()));

            if (result == 1)
                return ManageOperators();

            return Json(new
            {
                success = result
            });
        }
        
        private void SendRegistrationEmail(ApplicationUser opr, string tempPassword, string operatorid)
        {
            try
            {
                // send email to new operator
                var exaro = ConfigurationManager.AppSettings["companyEmail"];
                var friendlyName = ConfigurationManager.AppSettings["friendlyName"];
                string subject = "NHIS Iship portal login credentials";
                string message = $@"<div>Hello {opr.FirstName.ToTitleCase()} {opr.Surname.ToTitleCase()},</div>
                                <div style='margin-top: 10px'>You have been added as an operator on the NHIS Iship portal. 
                                Please log in with your email and this temporary password:</div>
                                <div style='font-size:18px; font-weight: bold; margin: 10px 0px'>{tempPassword}</div>
                                <div>You will be required to change your password on your first login.</div>
                                <div style='margin-top: 10px'>Thank you</div>";

                var emailcredentials = ConfigurationManager.AppSettings["email"];
                var emailpassword = Helper.DecryptConfig(ConfigurationManager.AppSettings["password"], false);

                using (var client = new SmtpClient("smtp.gmail.com", 587)
                {
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(emailcredentials, emailpassword)
                })
                {
                    var mailMessage = new MailMessage
                    {
                        From = new MailAddress(exaro, friendlyName),
                        IsBodyHtml = true
                    };
                    mailMessage.To.Add(opr.Email);
                    mailMessage.Body = message;
                    mailMessage.Subject = subject;
                    client.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {
                // If the email fails, don't fall over. Just let the operator resend the mail when it isn't delivered
                new AuthService().SaveError(ex, "Community/CreateOperator", operatorid);
            }
        }

        private MemberWrap SearchMemberOpr(SearchParam param)
        {
            if (param.IsPrint == 0)
                param.Page = 0;

            if (string.IsNullOrEmpty(param.OrderBy))
                param.OrderBy = "Name";

            if (param.PerPage == 0)
                param.PerPage = int.Parse(ConfigurationManager.AppSettings["auditPerPage"]);

            if (Session["searchParams"] is SearchParam search && param.IsPrint == 1)
            {
                param = search;
                param.IsPrint = 1;
            }

            var memberwrap = _data.SearchMembers(param);
            memberwrap.Page = new StaticPagedList<Member>(memberwrap.Members, 1, param.IsPrint == 1 && param.PrintPage == 0 ? memberwrap.Total : param.PerPage, memberwrap.Total);
            memberwrap.OrderBy = param.OrderBy;
            memberwrap.Order = param.Reverse == "1" ? "arrowup" : "arrowdown";
            memberwrap.Title = memberwrap.Total.ToString("N0") + " Records";

            memberwrap.CurrentPage = 1;

            if (param.IsPrint == 1)
            {
                memberwrap.IsPrint = true;
                string title = "Community Members";

                if (memberwrap.Members.Count != 0)
                {
                    if (!string.IsNullOrEmpty(param.Firstname))
                        title += " with Firstname \"" + memberwrap.Members[0].FirstName + "\"";

                    if (!string.IsNullOrEmpty(param.Surname))
                        title += " with Surname \"" + memberwrap.Members[0].Surname + "\"";

                    if (!string.IsNullOrEmpty(param.SearchGender))
                        title += " who is " + memberwrap.Members[0].Gender;


                    if (!string.IsNullOrEmpty(param.SearchLga))
                        title += " in " + memberwrap.Members[0].Lga;

                    if (!string.IsNullOrEmpty(param.SearchLga))
                        title += (string.IsNullOrEmpty(param.SearchLga) ? " in " : ", ") + memberwrap.Members[0].State;

                    if (!string.IsNullOrEmpty(param.SearchStartDate))
                        title += " Registered from " + DateTime.Parse(param.SearchStartDate).ToString("dd/MM/yyyy");

                    if (!string.IsNullOrEmpty(param.SearchEndDate))
                    {
                        title += (string.IsNullOrEmpty(param.SearchStartDate) ? " Registered till " : " to ") +
                                 DateTime.Parse(param.SearchEndDate).ToString("dd/MM/yyyy");
                    }
                }

                memberwrap.Title = title + $" ({memberwrap.Members.Count:N0})";
            }
            else
            {
                Session["searchParams"] = param;
            }

            return memberwrap;
        }
    }
}