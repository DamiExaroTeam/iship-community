﻿using Iship.Enums;
using Iship.Helpers;
using Iship.Models;
using Iship.Services;
using Iship.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using X.PagedList;

namespace Iship.Controllers
{
    [Authorized]
    public class UserManagementController : BaseController
    {
        private readonly AuthService _auth = new AuthService();

        public ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        [ClaimRequirement("UserManagement", "MyAccount")]
        public async Task<ActionResult> MyAccount()
        {
            var user = await UserManager.FindByIdAsync(int.Parse(User.Identity.GetUserId()));

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageMyAccount.cshtml", user)
            });
        }

        [ClaimRequirement("UserManagement", "ViewErrorLogs")]
        public ActionResult ViewErrorLogs()
        {
            var perpage = int.Parse(ConfigurationManager.AppSettings["errorPerPage"]);
            var errors = _auth.GetErrorLogs(perpage, 0, "TimeStamp", 1);

            errors.Page = new StaticPagedList<ErrorLog>(errors.ErrorLogs, 1, perpage, errors.Total);
            errors.OrderBy = "TimeStamp";
            errors.CurrentPage = 1;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewErrorLogs.cshtml", errors)
            });
        }

        [ClaimRequirement("UserManagement", "ViewAuditTrails")]
        public ActionResult ViewAuditTrails()
        {
            var perpage = int.Parse(ConfigurationManager.AppSettings["auditPerPage"]);
            var audits = _auth.GetAuditTrails(perpage, 0, "TimeStamp", 1);

            audits.Page = new StaticPagedList<Audit>(audits.Audits, 1, perpage, audits.Total);
            audits.OrderBy = "TimeStamp";
            audits.CurrentPage = 1;

            // Search values
            audits.UserList = _auth.GetUsers();
            audits.ActionsList = Enum.GetValues(typeof(EnumAuditEvents))
                .Cast<EnumAuditEvents>()
                .Select(v => v.ToString())
                .ToList();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewAuditTrails.cshtml", audits)
            });
        }
        
        [ClaimRequirement("UserManagement", "ManageOperators")]
        public ActionResult ManageOperators()
        {
            var oprtypes = new List<int>
            {
                (int)EnumOperatorType.CommunityUser,
                (int)EnumOperatorType.CommunityAdministrator,
                (int)EnumOperatorType.SystemReadOnly,
                (int)EnumOperatorType.SystemAdministrator
            };

            var operators = _auth.GetOperators(oprtypes, int.Parse(User.Identity.GetUserId()));
            
            // referenced in view
            ViewBag.Section = "UserManagement";

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Shared/_ManageOperatorsWrap.cshtml", operators)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOperators")]
        public ActionResult AddClaims(int id, string claims)
        {
            _auth.AddClaimsToUser(id, claims, int.Parse(User.Identity.GetUserId()));

            var model = GetUserClaims(id);
            model.Controller = "UserManagement";

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditUserClaimsModal.cshtml", model)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOperators")]
        public ActionResult RemoveClaims(int id, string claims)
        {
            _auth.RemoveClaimsFromUser(id, claims, int.Parse(User.Identity.GetUserId()));

            var model = GetUserClaims(id);
            model.Controller = "UserManagement";

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditUserClaimsModal.cshtml", model)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOperators")]
        public ActionResult GetUserPermissions(int id)
        {
            var claims = Helper.GetUserClaims(int.Parse(User.Identity.GetUserId()), id);
            claims.Controller = "UserManagement";

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Shared/_EditUserClaims.cshtml", claims)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOperators")]
        public ActionResult SetAdmin(int id, int status)
        {
            var result = _auth.SetOperatorAsAdmin(id, status, int.Parse(User.Identity.GetUserId()));

            if (result == 1)
                return ManageOperators();

            return Json(new
            {
                success = result
            });
        }

        [ClaimRequirement("UserManagement", "ManageOperators")]
        public ActionResult SetOperatorStatus(int id, int status)
        {
            var result = _auth.SetOperatorStatus(id, status, int.Parse(User.Identity.GetUserId()));

            if (result == 1)
                return ManageOperators();

            return Json(new
            {
                success = result
            });
        }

        [ClaimRequirement("UserManagement", "MyAccount")]
        public ActionResult UpdatePersonalDetails(ApplicationUser user)
        {
            // Confirm the correct user is making this change
            if (user.Id.ToString() != User.Identity.GetUserId())
                return Json(new
                {
                    success = 0
                });

            var result = _auth.UpdateUserDetails(user);

            if (result == -1)
                return Json(new
                {
                    success = result,
                    error = "That username is already in user. Please enter a different one"
                });

            return Json(new
            {
                success = result
            });
        }

        [ClaimRequirement("UserManagement", "MyAccount")]
        public async Task<ActionResult> ChangePassword(ApplicationUser user)
        {
            if (string.IsNullOrEmpty(user.CurrentPassword) || string.IsNullOrEmpty(user.NewPassword))
                return Json(new
                {
                    success = 0,
                    error = "Please enter both current and new password"
                });

            // Confirm the correct user is making this change
            if (user.Id.ToString() != User.Identity.GetUserId())
                return Json(new
                {
                    success = 0
                });

            _auth.UpdateUserPassword(user);

            var result = await UserManager.ChangePasswordAsync(int.Parse(user.Id.ToString()), user.CurrentPassword, user.NewPassword);

            if (result.Succeeded)
                return Json(new
                {
                    success = 1
                });

            return Json(new
            {
                success = 0,
                error = result.Errors.First()
            });
        }

        [ClaimRequirement("UserManagement", "ViewAuditTrails")]
        public ActionResult SearchAudit(int perpage, string userid, string action, string start, string end, string formno, string docno)
        {
            var audits = _auth.SearchAuditTrails(perpage, userid, action, start, end, formno, docno);
            audits.Page = new StaticPagedList<Audit>(audits.Audits, 1, perpage, audits.Total);
            audits.OrderBy = "TimeStamp";
            audits.CurrentPage = 1;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewAuditTrailsResult.cshtml", audits)
            });
        }

        [ClaimRequirement("UserManagement", "ViewErrorLogs")]
        public ActionResult GetInnerException(int id)
        {
            var error = _auth.GetErrorLog(id);

            return Json(new
            {
                success = 1,
                msg = error.Innermessage
            });
        }

        [ClaimRequirement("UserManagement", "ViewErrorLogs")]
        public ActionResult GetErrorLogsPage(int page, string orderby, int perpage, int reverse)
        {
            var errors = _auth.GetErrorLogs(perpage, page - 1, orderby, reverse);
            errors.Page = new StaticPagedList<ErrorLog>(errors.ErrorLogs, page, perpage, errors.Total);
            errors.OrderBy = orderby;
            errors.CurrentPage = page;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewErrorLogsResult.cshtml", errors)
            });
        }

        [ClaimRequirement("UserManagement", "ViewAuditTrails")]
        public ActionResult GetAuditTrailPage(int page, string orderby, int perpage, int reverse)
        {
            var audits = _auth.GetAuditTrails(perpage, page - 1, orderby, reverse);
            audits.Page = new StaticPagedList<Audit>(audits.Audits, page, perpage, audits.Total);
            audits.OrderBy = orderby;
            audits.CurrentPage = page;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewAuditTrailsResult.cshtml", audits)
            });
        }

        private EditUserClaimsWrap GetUserClaims(int id)
        {
            var claims = new List<EditUserClaims>();
            var user = UserManager.FindById(id);
            var userclaims = UserManager.GetClaims(user.Id);
            var allclaims = _auth.GetUserClaims();

            // selected
            foreach (var claim in userclaims)
            {
                var userclaim = claims.FirstOrDefault(x => x.Controller == claim.Type);

                if (userclaim == null)
                {
                    userclaim = new EditUserClaims
                    {
                        Controller = claim.Type
                    };

                    claims.Add(userclaim);
                }

                userclaim.SelectedClaims.Add(claim.Value);
            }

            // unselected
            foreach (var claim in allclaims)
            {
                var userclaim = claims.FirstOrDefault(x => x.Controller == claim.Controller);
                if (userclaim == null)
                {
                    userclaim = new EditUserClaims
                    {
                        Controller = claim.Controller
                    };

                    claims.Add(userclaim);
                }

                if (userclaim.Controller == claim.Controller && !userclaim.SelectedClaims.Contains(claim.Action))
                    userclaim.UnSelectedClaims.Add(claim.Action);
            }

            // order them
            foreach (var c in claims)
            {
                c.SelectedClaims = c.SelectedClaims.OrderBy(x => x).ToList();
                c.UnSelectedClaims = c.UnSelectedClaims.OrderBy(x => x).ToList();
            }

            return new EditUserClaimsWrap
            {
                UserId = id.ToString(),
                Claims = claims
            };
        }
    }
}