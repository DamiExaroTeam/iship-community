﻿using Iship.Helpers;
using Iship.Models.Account;
using Iship.Services;
using Iship.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Iship.Controllers
{
    public class HomeController : BaseController
    {
        public ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Tools", "Home");

            var model = new RegistrationModel();

            if (Request.Url != null && Request.Url.Host.StartsWith("localhost"))
            {
                model.LoginViewModel = new LoginViewModel
                {
                    Email = "dlawal@irissmart.com",
                    Password = "damilola"
                };
            }

            return View(model);
        }

        public ActionResult Error()
        {
            if (User.Identity.IsAuthenticated && !Request.IsAjaxRequest())
            {
                HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                new AuthService().UserLoggedOut(int.Parse(User.Identity.GetUserId()));
            }

            return View();
        }

        public ActionResult ErrorHandled(int code)
        {
            return new HttpStatusCodeResult(code);
        }

        [Authorized]
        public async Task<ActionResult> Tools()
        {
            // new AuthService().FixCommunities();

            var signedUser = await UserManager.FindByIdAsync(int.Parse(User.Identity.GetUserId()));

            if (signedUser == null)
            {
                HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return RedirectToAction(nameof(Index), "Home");
            }


            var user = new AuthService().GetUserById(int.Parse(User.Identity.GetUserId()));
            //var dashboard = new DataService().GetDashboardData(user.OperatorType);

            // Get user claims
            var auth = new AuthService();

            var claims = auth.GetUserClaims(signedUser.Id);
            var sections = auth.GetSections();

            // assign claims to sections
            foreach (var section in sections)
            {
                foreach (var subsection in section.SubSections)
                {
                    subsection.Claims.AddRange(claims.Where(x => x.Controller == section.Name && x.SubSectionId == subsection.Id));
                }
            }

            var menuModel = new MenuModel
            {
                Username = signedUser.UserName,
                Sections = sections,
                Claims = claims.OrderBy(x => x.Action).ToList(),
                //Dashboard = dashboard
            };

            return View(menuModel);
        }
    }
}