﻿using Iship.Helpers;
using Iship.Services;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace Iship.Controllers
{
    [Authorized]
    public class DashboardController : BaseController
    {
        public ActionResult GetDashboard()
        {
            var user = new AuthService().GetUserById(int.Parse(User.Identity.GetUserId()));
            var dashboardinfo = new DataService().GetDashboardData(user.OperatorType);

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Shared/_Dashboard.cshtml", dashboardinfo)
            });
        }
    }
}