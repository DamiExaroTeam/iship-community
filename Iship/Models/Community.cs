﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class Community
    {
        public int CommunityId { get; set; }

        [StringLength(100)]
        public string CommunityName { get; set; }

        [StringLength(500)]
        public string CommunityDesc { get; set; }

        public bool ShowCommunity { get; set; }

        public string CommunityState { get; set; }

        public string CommunityLga { get; set; }

        public int LkpStatus { get; set; }

        public bool Deactivated { get; set; }

        public string Status { get; set; }

        public int FkMchId { get; set; }

        public DateTime SysRegDate { get; set; }

        [StringLength(20)]
        public string SysRegUser { get; set; }

        public DateTime SysEditDate { get; set; }

        [StringLength(20)]
        public string SysEditUser { get; set; }

        [StringLength(5)]
        public string FkTaskcode { get; set; }

        public int FkGeoarea { get; set; }

        public int StateId { get; set; }

        [StringLength(100)]
        public string GeoAreaDesc { get; set; }

        [StringLength(100)]
        public string PoliticalZone { get; set; }

        public DateTime LaunchedDate { get; set; }

        [StringLength(20)]
        public string MhaCode { get; set; }

        public int FkHcp { get; set; }

        public List<Member> Members { get; set; }

        // Creation and edit
        public List<GeoArea> States { get; set; }

        public List<GeoArea> Lgas { get; set; }

        public List<Community> Communities { get; set; }

        public int GeoArea { get; set; }
    }
}