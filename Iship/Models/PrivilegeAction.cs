﻿using System;

namespace Iship.Models
{
    public class PrivilegeAction
    {
        public int Id { get; set; }

        public int PrivilegeId { get; set; }

        public bool IsActive { get; set; } = true;

        public DateTime DateCreated { get; set; } = DateTime.Now;
    }
}
