﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class Section
    {
        public Section()
        {
            SubSections = new List<SubSection>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public string Icon { get; set; }

        public List<SubSection> SubSections;
    }
}