﻿using System;

namespace Iship.Models
{
    public class ErrorLog
    {
        public int ExceptionLogId { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string Machinenameorip { get; set; }

        public string Innermessage { get; set; }

        public string Browser { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public string Innerstacktrace { get; set; }

        public DateTime ExceptionDate { get; set; }

        public string Firstname { get; set; }

        public string Surname { get; set; }
    }
}
