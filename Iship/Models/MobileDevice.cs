﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class MobileDevice
    {
        public int MdId { get; set; }

        [StringLength(50)]
        public string Model { get; set; }

        [StringLength(250)]
        public string Serialnumber { get; set; }

        public int FlgBlacklisted { get; set; }

        [StringLength(100)]
        public int BlacklistedReason { get; set; }

        public DateTime DateLastused { get; set; }

        public int FkLastoperator { get; set; }

        public int SysDelete { get; set; }

        public int SysActive { get; set; }

        public DateTime SysRegDate { get; set; }

        [StringLength(50)]
        public string SysRegUser { get; set; }

        public DateTime SysEditDate { get; set; }

        [StringLength(50)]
        public int SysEditUser { get; set; }

        public string Lifeline { get; set; }

        [StringLength(5)]
        public int FkTaskcode { get; set; }

        public int FkProgramOwner { get; set; }

        [StringLength(50)]
        public int Platform { get; set; }

        [StringLength(200)]
        public int MacAddress { get; set; }

        [StringLength(20)]
        public int ProcessorId { get; set; }
    }
}