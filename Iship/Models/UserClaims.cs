﻿using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class UserClaim
    {
        public UserClaim()
        {
            SubSection = new SubSection();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Controller { get; set; }

        [Required]
        [StringLength(50)]
        public string Action { get; set; }

        public int SubSectionId { get; set; }

        public bool Visible { get; set; }

        public SubSection SubSection { get; set; }
    }
}