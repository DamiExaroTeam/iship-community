﻿using System;

namespace Iship.Models
{
    public class Delivery
    {
        public int DeliveryId { get; set; }

        public int MealPortion { get; set; }

        public int LkpMealType { get; set; }

        public int MealCount { get; set; }

        public int FkSchoolId { get; set; }

        public int FkPersonnelId { get; set; }

        public byte[] PersonnelThumb { get; set; }

        public byte[] PersonnelSign { get; set; }

        public int FkVendorId { get; set; }

        public int FkRepId { get; set; }

        public byte[] RepThumb { get; set; }

        public byte[] RepSign { get; set; }

        public int DeviceRecno { get; set; }

        public DateTime SysRegDate { get; set; }
    }
}