﻿using Iship.Enums;
using Iship.Extensions;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class AuditEvent
    {
        public AuditEvent() { } 

        public AuditEvent(EnumAuditEvents @enum)
        {
            Id = (int)@enum;
            Name = @enum.ToString();
            Description = @enum.GetEnumDescription();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }
    }
}
