﻿using Iship.Extensions;
using Microsoft.AspNet.Identity;
using Oracle.Identity.Dapper;
using System.ComponentModel;
using System.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Iship.Models
{
    public class ApplicationUser : IdentityMember
    {
        [DefaultValue(1)]
        public bool IsFirstTimeLogin { get; set; }

        public string NewPassword { get; set; }

        public int OperatorType { get; set; }

        public bool IsAdmin { get; set; }

        public string OperatorTypeName { get; set; }

        public string CurrentPassword { get; set; }

        public string NewPasswordConfirm { get; set; }

        public string Section { get; set; }

        public string GetFullName()
        {
            return FirstName.ToTitleCase() + " " + Surname.ToTitleCase();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : DbManager
    {
        public ApplicationDbContext(string connectionName)
            : base(connectionName)
        {
        }

        public static ApplicationDbContext Create()
        {
            //var conn = ConfigurationManager.AppSettings["useLaptopAsCentral"].ToLower() == "true"
            //    ? "AuthLocal"
            //    : "Auth";
            return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["Iship"].ConnectionString);
        }
    }
}
