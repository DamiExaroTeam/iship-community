﻿using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class State
    {
        public int StateId { get; set; }

        [StringLength(3)]
        public string StateCode { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
    }
}
