﻿using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class Title
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(1)]
        public string Gender { get; set; }
    }
}
