﻿using System.Collections.Generic;
using Iship.ViewModels;

namespace Iship.Models
{
    public class Dashboard
    {
        public Dashboard()
        {
            Kpis = new List<KpiWrap>();
            Alerts = new List<KpiWrap>();
            Graphs = new List<Graph>();
        }

        public List<KpiWrap> Kpis { get; set; }

        public List<KpiWrap> Alerts { get; set; }

        public List<Graph> Graphs { get; set; }

        public int SearchType { get; set; }

        public int NhisNumber { get; set; }

        public string ReferenceNumber { get; set; }

        public string Firstname { get; set; }

        public string Surname { get; set; }
    }
}