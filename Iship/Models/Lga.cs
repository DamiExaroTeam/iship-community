﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class Lga
    {
        public int LgaId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(3)]
        public string SysDelete { get; set; }

        [StringLength(3)]
        public string SysActive { get; set; }

        public DateTime SysRegDate { get; set; }

        [StringLength(50)]
        public string SysRegUser { get; set; }

        public DateTime SysEditDate { get; set; }

        [StringLength(50)]
        public string SysEditUser { get; set; }

        [StringLength(18)]
        public int FkStateId { get; set; }
    }
}