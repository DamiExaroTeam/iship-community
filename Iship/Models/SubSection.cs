﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class SubSection
    {
        public SubSection()
        {
            Claims = new List<UserClaim>();
        }

        public int Id { get; set; }

        public int SectionId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public List<UserClaim> Claims { get; set; }
    }
}
