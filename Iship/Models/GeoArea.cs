﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Iship.Models
{
    public class GeoArea
    {
        public int GaId { get; set; }

        public int FkParentId { get; set; }

        [StringLength(50)]
        public string GaName { get; set; }

        [StringLength(5)]
        public string GaCode { get; set; }

        public int LkpGaType { get; set; }

        public int FkSource { get; set; }

        [StringLength(5)]
        public string SysDelete { get; set; }

        [StringLength(5)]
        public string SysActive { get; set; }

        public DateTime SysRegDate { get; set; }

        [StringLength(50)]
        public string SysRegUser { get; set; }

        public DateTime SysEditDate { get; set; }

        [StringLength(50)]
        public string SysEditUser { get; set; }

        public string Hierarchy { get; set; }

        [StringLength(5)]
        public string FkTaskcode { get; set; }
    }
}