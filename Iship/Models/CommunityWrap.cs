﻿using System.Collections.Generic;
using X.PagedList;

namespace Iship.Models
{
    public class CommunityWrap
    {
        public List<Community> Communities { get; set; }

        public string Title { get; set; }

        public bool IsPrint { get; set; }

        public int CurrentPage { get; set; }

        public int NumberOfPages { get; set; }

        public int NumberPerPage { get; set; }

        public int Reverse { get; set; }

        public string OrderBy { get; set; }

        public string Order { get; set; }

        public int Total { get; set; }

        public StaticPagedList<Community> Page { get; set; }

        public string SearchName { get; set; }

        public string SearchStartDate { get; set; }

        public string SearchEndDate { get; set; }

        public List<GeoArea> Lgas { get; set; }

        public List<GeoArea> States { get; set; }
    }
}