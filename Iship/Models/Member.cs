﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class Member
    {
        public int MemberId { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string Surname { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string State { get; set; }

        public string Lga { get; set; }

        public int LkpGender { get; set; }

        public string Gender { get; set; }

        public string CommunityName { get; set; }

        [StringLength(10)]
        public string Grade { get; set; }

        public int LkpStatus { get; set; }

        public string Status { get; set; }

        public int LkpMemberType { get; set; }

        public DateTime RegistrationDate { get; set; }

        public int LkpMarStatus { get; set; }

        [StringLength(20)]
        public string NationalId { get; set; }

        [StringLength(100)]
        public string EmailAddress { get; set; }

        [StringLength(20)]
        public string GsmNumber { get; set; }

        public int FlgAssumedDob { get; set; }

        public int FkOperatorEnroll { get; set; }

        public int FkDeviceEnroll { get; set; }

        public int FkInsId { get; set; }

        public int DeviceRecno { get; set; }

        public int UploadSeq { get; set; }

        public int FlgDuplicate { get; set; }

        public int SysDelete { get; set; }

        public int SysActive { get; set; }

        public DateTime SysRegDate { get; set; }

        [StringLength(20)]
        public string SysRegUser { get; set; }

        public DateTime SysEditDate { get; set; }

        [StringLength(20)]
        public string SysEditUser { get; set; }

        public byte[] FacialImage { get; set; }

        public byte[] ThumbRight { get; set; }

        public byte[] ThumbLeft { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [StringLength(500)]
        public string RejectReason { get; set; }
       
        public string Lifeline { get; set; }

        [StringLength(5)]
        public string FkTaskcode { get; set; }

        public int FkMemberLink { get; set; }

        public int FkCommunity { get; set; }

        public int Lkp2Ndmembertype { get; set; }

        public int FkPregnancy { get; set; }

        public bool FlgPregnantReg { get; set; }

        public DateTime DatePregnantReg { get; set; }

        public DateTime LmpDate { get; set; }

        public byte[] Signature { get; set; }

        public DateTime BiometricUpdateDate { get; set; }

        public int FkHcp { get; set; }

        public int Weight { get; set; }

        public int Height { get; set; }

        [StringLength(5)]
        public string RefNo { get; set; }

        public int NhisId { get; set; }

        public byte[] BirthCert { get; set; }

        public bool Nsfpuploaded { get; set; }

        public DateTime Nsfpdate { get; set; }

        [StringLength(50)]
        public string Nsfptransno { get; set; }

        public bool FlgFacialRec { get; set; }

        public Institution Institution { get; set; }
    }
}