﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class Hmo
    {
        public int HmoId { get; set; }

        [StringLength(60)]
        public string HmoName { get; set; }

        [StringLength(60)]
        public string HmoAddrLn1 { get; set; }

        [StringLength(60)]
        public string HmoAddrLn2 { get; set; }

        [StringLength(20)]
        public string HmoTel { get; set; }

        [StringLength(24)]
        public string HmoShortName { get; set; }

        [StringLength(20)]
        public string HmoCallCentNo { get; set; }

        public bool SysDelete { get; set; }

        public bool SysActive { get; set; }

        public DateTime SysRegDate { get; set; }

        [StringLength(50)]
        public string SysRegUser { get; set; }

        public DateTime SysEditDate { get; set; }

        [StringLength(50)]
        public string SysEditUser { get; set; }
    }
}