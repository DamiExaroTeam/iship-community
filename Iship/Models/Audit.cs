﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class Audit
    {
        public int Id { get; set; }

        public AuditEvent Event { get; set; }

        public int EventId { get; set; }

        public int ItemId { get; set; }

        public string Description { get; set; }

        [DefaultValue("getdate()")]
        public DateTime TimeStamp { get; set; }

        public ApplicationUser User { get; set; }

        public int UserId { get; set; }

        [StringLength(12)]
        public string FormNo { get; set; }

        [StringLength(12)]
        public string DocNo { get; set; }
    }
}
