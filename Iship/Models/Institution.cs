﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class Institution
    {
        public int InsId { get; set; }

        [StringLength(100)]
        public string InsName { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        public int LkpType { get; set; }

        public int LkpStatus { get; set; }

        public int FkHmoId { get; set; }

        public int FkHcpId { get; set; }

        [StringLength(50)]
        public string ContactPerson { get; set; }

        [StringLength(20)]
        public string ContactNumber { get; set; }

        public int Longitude { get; set; }

        public int Latitude { get; set; }

        public int FkGaId { get; set; }

        public DateTime RegistrationDate { get; set; }

        [StringLength(3)]
        public string SysDelete { get; set; }

        [StringLength(3)]
        public string SysActive { get; set; }

        public DateTime SysRegDate { get; set; }

        [StringLength(50)]
        public string SysRegUser { get; set; }

        public DateTime SysEditDate { get; set; }

        [StringLength(50)]
        public string SysEditUser { get; set; }

        [StringLength(20)]
        public string Abbreviation { get; set; }

        public DateTime FkDeviceEnroll { get; set; }

        public int FkOperatorEnroll { get; set; }

        public int Estpopulation { get; set; }

        public int FkGaIdSec { get; set; }

        [StringLength(5)]
        public string FkTaskcode { get; set; }

        public string Lifeline { get; set; }

    }
}