﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Iship.Models
{
    public class Hcp
    {
        public int HcpId { get; set; }

        [StringLength(10)]
        public string HcpCd { get; set; }

        [StringLength(100)]
        public string HcpName { get; set; }

        [StringLength(200)]
        public string Addr { get; set; }

        [StringLength(200)]
        public string TypSrvc { get; set; }

        [StringLength(20)]
        public string TelNum { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(24)]
        public string HcpShortName { get; set; }

        [StringLength(50)]
        public string HcpCallCentreNumber { get; set; }

        [StringLength(50)]
        public string PublicEmail { get; set; }

        [StringLength(50)]
        public string Website { get; set; }

        public int HcpAdminId { get; set; }

        public int AltHcpAdminId { get; set; }

        public int FkLgaId { get; set; }

        public int Chgnum { get; set; }

        public DateTime ChangeDate { get; set; }

        public int Latitude { get; set; }

        public int Longitude { get; set; }

        public int LkpSector { get; set; }

        public int LkpOwnership { get; set; }

        public int FlgAccredited { get; set; }

        public int OfficialCode { get; set; }

        [StringLength(3)]
        public string SysDelete { get; set; }

        [StringLength(3)]
        public string SysActive { get; set; }

        public DateTime SysRegDate { get; set; }

        [StringLength(50)]
        public string SysRegUser { get; set; }

        public DateTime SysEditDate { get; set; }

        [StringLength(50)]
        public string SysEditUser { get; set; }

        public int FkHcpId { get; set; }

        public int FkGeoarea { get; set; }

        [StringLength(5)]
        public string FkTaskcode { get; set; }

        public string Lifeline { get; set; }
    }
}