﻿
namespace Iship.Models.Account
{
    public class RegistrationModel
    {
        public RegistrationModel()
        {
            LoginViewModel = new LoginViewModel();
            ForgotPasswordViewModel = new ForgotPasswordViewModel();
            ChangePasswordViewModel = new ChangePasswordViewModel();;
        }

        public LoginViewModel LoginViewModel { get; set; }

        public ChangePasswordViewModel ChangePasswordViewModel { get; set; }

        public RegisterViewModel RegisterViewModel { get; set; }

        public ForgotPasswordViewModel ForgotPasswordViewModel { get; set; }
    }
}
