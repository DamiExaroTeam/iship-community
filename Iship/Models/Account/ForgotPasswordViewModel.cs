﻿using System.ComponentModel.DataAnnotations;

namespace Iship.Models.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
