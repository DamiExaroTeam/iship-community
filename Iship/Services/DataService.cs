﻿using Dapper;
using Iship.Enums;
using Iship.Extensions;
using Iship.Models;
using Iship.ViewModels;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Iship.Services
{
    [System.Runtime.InteropServices.Guid("868C586B-43EE-4E95-9453-ED7F64677D00")]
    public class DataService
    {
        private readonly string _conn = ConfigurationManager.ConnectionStrings["Iship"].ConnectionString;

        // Data
        public Dashboard GetDashboardData(int oprType)
        {
            var dashboard = new Dashboard();

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                string ok = "greenalert";
                string warning = "pinkalert";
                string critical = "redalert";

                // Get various KPIs

                var systemadmin = oprType == (int) EnumOperatorType.SystemAdministrator || oprType == (int) EnumOperatorType.SystemReadOnly;
                if (oprType == (int)EnumOperatorType.PrimarySchoolsAdministrator || oprType == (int)EnumOperatorType.PrimarySchoolsUser || systemadmin)
                {
                    var scholarsthismonth = GetMembersThisMonth((int)EnumMemberType.Scholars, true);

                    var scholarstoday = scholarsthismonth.Count(x => x.SysRegDate.Date == DateTime.Today);
                    var scholarsyesterday = scholarsthismonth.Count(x => x.SysRegDate.Date == DateTime.Today.AddDays(-1));
                    var thisweek = scholarsthismonth.Count(x => x.SysRegDate.Date == DateTime.Today.StartOfWeek(DayOfWeek.Sunday));

                    var schoolskpi = new List<Kpi>
                    {
                        new Kpi
                        {
                            Name = "Scholars in Temporary table",
                            Value1 = dbConn.Query<int>("select count(1) from tmp_member where lkp_member_type = 16").FirstOrDefault()
                                .ToString("N0"),
                            Status = ok,
                            ViewUrl = "/PrimarySchools/ViewTemporaryScholars"
                        },
                        new Kpi
                        {
                            Name = "Scholars registered",
                            Value1 = dbConn.Query<int>("select count(1) from member where lkp_member_type = 16").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "/PrimarySchools/ViewScholars"
                        },
                        new Kpi
                        {
                            Name = "Scholars registered",
                            Value1 = dbConn.Query<int>("select count(1) from member where lkp_member_type = 16").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "/PrimarySchools/ViewScholars"
                        },
                        new Kpi
                        {
                            Name = "Schools with registered scholars",
                            Value1 = dbConn.Query<int>(@"select COUNT(1) from institution 
                                                        where ins_id in (select distinct nvl(fk_ins_id,0) from member where lkp_member_type = 16)").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "/PrimarySchools/ViewSchools?hasScholars=1"
                        },
                        new Kpi
                        {
                            Name = "Schools without registered scholars",
                            Value1 = dbConn.Query<int>(@"SELECT COUNT(1) from institution 
                                                        where ins_id not in (select distinct nvl(fk_ins_id,0) from member where lkp_member_type = 16)").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "/PrimarySchools/ViewSchools?hasScholars=0"
                        },
                        new Kpi
                        {
                            Name = "Scholars registered today",
                            Value1 = scholarstoday.ToString("N0"),
                            Status = critical,
                            ViewUrl = "/PrimarySchools/ViewScholars?reg=today"
                        },
                        new Kpi
                        {
                            Name = "Scholars registered yesterday",
                            Value1 = scholarsyesterday.ToString("N0"),
                            Status = critical,
                            ViewUrl = "/PrimarySchools/ViewScholars?reg=yesterday"
                        },
                        new Kpi
                        {
                            Name = "Scholars registered this week",
                            Value1 = thisweek.ToString("N0"),
                            Status = critical,
                            ViewUrl = "/PrimarySchools/ViewScholars?reg=thisweek"
                        },
                        new Kpi
                        {
                            Name = "Scholars registered this month",
                            Value1 = scholarsthismonth.Count.ToString("N0"),
                            Status = critical,
                            ViewUrl = "/PrimarySchools/ViewScholars?reg=thismonth"
                        }
                    };
                    
                    var schoolalerts = new List<Kpi>
                    {
                        new Kpi
                        {
                            Name = "Duplicate scholars resolved",
                            Value1 = dbConn.Query<int>(@"select count(1) from member_dupl").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "/PrimarySchools/ViewDuplicates"
                        },
                        new Kpi
                        {
                            Name = "Double upload yet to be resolved",
                            Value1 = dbConn.Query<int>(@"select count(1) from member_multiple").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "/PrimarySchools/ViewMultipleUploads"
                        },
                        new Kpi
                        {
                            Name = "Schools linked to a Ward",
                            Value1 = dbConn.Query<int>(@"select count(1) from institution left join geoarea ON ga_id = fk_ga_id where lkp_ga_type <> 3").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "/PrimarySchools/ViewSchools?hasWards=1"
                        },
                        new Kpi
                        {
                            Name = "HCPs linked to a Ward",
                            Value1 = dbConn.Query<int>(@"select count(1) from institution, (select distinct fk_ins_id FROM member WHERE sys_delete = 0 AND sys_active = 1) t2
                                                               where institution.ins_id = t2.fk_ins_id AND fk_hcp_id IS NULL").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "/PrimarySchools/ViewSschools?hasScholars=0"
                        },
                        new Kpi
                        {
                            Name = "Schools linked to HCP",
                            Value1 = dbConn.Query<int>(@"SELECT COUNT(1) from institution, (select distinct fk_ins_id from member where sys_delete = 0 and sys_active = 1) t2
                                                                where institution.ins_id = t2.fk_ins_id and fk_hcp_id is null").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "/PrimarySchools/ViewSschools?hasScholars=0"
                        }
                    };

                    var schoolwrap = new KpiWrap
                    {
                        SectionId = 2,
                        Title = "Primary School KPIs",
                        Kpis = new List<Kpi>(schoolskpi),
                        DisableView = true
                    };
                    dashboard.Kpis.Add(schoolwrap);

                    var schoolalertwrap = new KpiWrap
                    {
                        SectionId = 2,
                        Title = "Primary School Alerts",
                        Kpis = new List<Kpi>(schoolalerts),
                        DisableView = true
                    };
                    dashboard.Alerts.Add(schoolalertwrap);
                }

                if (oprType == (int)EnumOperatorType.CommunityAdministrator || oprType == (int)EnumOperatorType.CommunityUser || systemadmin)
                {
                    var communitymembersthismonth = GetMembersThisMonth((int)EnumMemberType.Community, true);

                    var communitymemberstoday = communitymembersthismonth.Count(x => x.SysRegDate.Date == DateTime.Today);
                    var communitymembersyesterday = communitymembersthismonth.Count(x => x.SysRegDate.Date == DateTime.Today.AddDays(-1));
                    var communitymembersthisweek = communitymembersthismonth.Count(x => x.SysRegDate.Date == DateTime.Today.StartOfWeek(DayOfWeek.Sunday));

                    var communitykpis = new List<Kpi>
                    {
                        new Kpi
                        {
                            Name = "Communities registered",
                            Value1 = dbConn.Query<int>("select count(1) from community").FirstOrDefault()
                                .ToString("N0"),
                            Status = ok,
                            ViewUrl = "data-sectiontitle=Communitiessubsection data-section=Community data-option=ViewCommunities"
                        },
                        new Kpi
                        {
                            Name = "Community members registered",
                            Value1 = dbConn.Query<int>("select count(1) from member where lkp_member_type = 78").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "data-sectiontitle=Memberssubsection data-section=Community data-option=ViewMembers"
                        },
                        new Kpi
                        {
                            Name = "Community members in Temporary table",
                            Value1 = dbConn.Query<int>("select count(1) from tmp_member where lkp_member_type = 78").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "data-section=community data-subsection=Members data-option=ViewTemporaryMembers"
                        },
                        new Kpi
                        {
                            Name = "HCPs registered",
                            Value1 = dbConn.Query<int>("select count(1) from hcp").FirstOrDefault()
                                .ToString("N0"),
                            Status = ok,
                            ViewUrl = "" // "/HCPs/ViewHCPs"
                        },
                        new Kpi
                        {
                            Name = "HMOs registered",
                            Value1 = dbConn.Query<int>("select count(1) from hmo").FirstOrDefault().ToString("N0"),
                            Status = ok,
                            ViewUrl = "" // "/HMOs/ViewHMOs"
                        },
                        new Kpi
                        {
                            Name = "Community members registered today",
                            Value1 = communitymemberstoday.ToString("N0"),
                            Status = critical,
                            ViewUrl = "data-section=community data-subsection=Members data-option=ViewMembers?reg=today"
                        },
                        new Kpi
                        {
                            Name = "Community members yesterday",
                            Value1 = communitymembersyesterday.ToString("N0"),
                            Status = critical,
                            ViewUrl = "data-section=community data-subsection=Members data-option=ViewMembers?reg=yesterday"
                        },
                        new Kpi
                        {
                            Name = "Community members this week",
                            Value1 = communitymembersthisweek.ToString("N0"),
                            Status = critical,
                            ViewUrl = "data-section=community data-subsection=Members data-option=ViewMembers?reg=thisweek"
                        },
                        new Kpi
                        {
                            Name = "Community members this month",
                            Value1 = communitymembersthismonth.Count.ToString("N0"),
                            Status = critical,
                            ViewUrl = "data-section=community data-subsection=Members data-option=ViewMembers?reg=thismonth"
                        }
                    };

                    var communitywrap = new KpiWrap
                    {
                        SectionId = 1,
                        Title = "Community KPIs",
                        Kpis = new List<Kpi>(communitykpis)
                    };
                    dashboard.Kpis.Add(communitywrap);

                    int sum = 0;

                    var communityYearData = GetCommunityLastYear();
                    var totalcomreg = new Graph
                    {
                        Name = "totalcomunityreg",
                        Title = "Total number of communities registered",
                        Title2 = "This year",
                        Section = "Community",
                        SectionId = 1,
                        High = Convert.ToInt32(communityYearData.Y.Max() * 1.25),
                        Labels = string.Join(", ", communityYearData.X.Select(x => $"{x}")),
                        Series = string.Join(", ", communityYearData.Y.GetRange(0, DateTime.Today.Month).Select(x => sum += x).ToArray()),
                        YAxis = "Communitites",
                        XAxis = "Month",
                        Color = "green"
                    };
                    dashboard.Graphs.Add(totalcomreg);

                    var communityMembersYearData = GetMembersLastYear((int)EnumMemberType.Community);
                    var totalreg = new Graph
                    {
                        Name = "totalcomreg",
                        Title = "Total number of community members registered",
                        Title2 = "This year",
                        Section = "Community",
                        SectionId = 1,
                        High = Convert.ToInt32(communityMembersYearData.Y.Max() * 1.25),
                        Labels = string.Join(", ", communityMembersYearData.X.Select(x => $"{x}")),
                        Series = string.Join(", ", communityMembersYearData.Y.GetRange(0, DateTime.Today.Month).Select(x => sum += x).ToArray()),
                        YAxis = "Members",
                        XAxis = "Month",
                        Color = "green"
                    };
                    dashboard.Graphs.Add(totalreg);

                    var communitymonthdata = GetMembersThisMonth((int)EnumMemberType.Community);
                    var monthreg = new Graph
                    {
                        Name = "monthcomreg",
                        Title = "Community members registered this month",
                        Title2 = "Till today",
                        Section = "Community",
                        SectionId = 1,
                        High = Convert.ToInt32(communitymonthdata.Y.Max() * 1.25),
                        Labels = string.Join(", ", communitymonthdata.X),
                        Series = string.Join(", ", communitymonthdata.Y.GetRange(0, DateTime.Today.Day)),
                        YAxis = "Members",
                        XAxis = "Day",
                        Color = "green"
                    };
                    dashboard.Graphs.Add(monthreg);
                }

                if (systemadmin)
                {
                    var adminkpis = new List<Kpi>
                    {
                        new Kpi
                        {
                            Name = "Active devices",
                            Value1 = dbConn.Query<int>("select count(1) from mobile_device where flg_blacklisted = 0 or sys_active = 1 ").FirstOrDefault()
                                .ToString("N0"),
                            Status = ok,
                            ViewUrl = "/Devices/ViewDevices"
                        }
                    };

                    var communitykpi = dashboard.Kpis.First(x => x.SectionId == 1);
                    communitykpi.Kpis.AddRange(adminkpis);
                }
            }

            return dashboard;
        }

        public TableResult GetScholars()
        {
            var result = new TableResult();
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var members = dbConn.Query<Member, Institution, Member>(
                    "select SELECT MEMBER_ID, FIRST_NAME, SURNAME, DATE_OF_BIRTH, INS_ID, INS_NAME, ADDRESS from member m left join institution i on m.fk_ins_id = i.ins_id where LKP_MEMBER_TYPE = 16",
                    (member, school) =>
                    {
                        member.Institution = school;
                        return member;
                    },
                    splitOn: "member_id, ins_id").ToList();

                result.Headers = new List<string>
                {
                    "No", "Firstname", "Surname", "Time Registered", "School"
                };

                for (int i = 0; i < members.Count; i++)
                {
                    result.Rows.Add(new List<string>
                    {
                        (i+1).ToString(),
                        members[i].FirstName,
                        members[i].Surname,
                        members[i].SysRegDate.ToString("dd/MM/yyyy hh:mm tt").ToLower(),
                        members[i].Institution?.InsName
                    });
                }
            }

            return result;
        }

        public List<GeoArea> GetStates()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                return dbConn.Query<GeoArea>("select ga_id, ga_name from geoarea where Lkp_ga_type=1 and sys_active = 1 order by ga_name").ToList();
            }
        }

        public List<GeoArea> GetLgas()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                return dbConn.Query<GeoArea>("select ga_id, ga_name, fk_parent_id from geoarea where Lkp_ga_type=2 and sys_active = 1 order by ga_name").ToList();
            }
        }

        // Communities
        public List<Community> GetCommunities()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                return dbConn.Query<Community>(@"SELECT COMMUNITY_ID, 
                        COMMUNITY_NAME, 
                        COMMUNITY_DESC,
                        SYS_REG_dATE,
                        (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = COMMUNITY.LKP_STATUS) as LKP_STATUS, 
                        MHA_CODE,
                        (select (select GA.GA_Name from GEOAREA GA where GA.GA_ID = EXTRACTVALUE(GEOAREA.HIERARCHY, '/structure/state')) from GEOAREA where GA_ID = COMMUNITY.FK_GEOAREA) CommunityState,
                        (select (select GA.GA_Name from GEOAREA GA where GA.GA_ID = EXTRACTVALUE(GEOAREA.HIERARCHY, '/structure/lga')) from GEOAREA where GA_ID = COMMUNITY.FK_GEOAREA) CommunityLGA    
                        FROM COMMUNITY").ToList();
            }
        }

        public CommunityWrap GetCommunities(int perpage, int page, string orderby, int reverse)
        {
            var query = @"SELECT COMMUNITY_ID, 
                        COMMUNITY_NAME, 
                        COMMUNITY_DESC,
                        SYS_REG_dATE,
                        (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = COMMUNITY.LKP_STATUS) as LKP_STATUS, 
                        MHA_CODE,
                        (select (select GA.GA_Name from GEOAREA GA where GA.GA_ID = EXTRACTVALUE(GEOAREA.HIERARCHY, '/structure/state')) from GEOAREA where GA_ID = COMMUNITY.FK_GEOAREA) CommunityState,
                        (select (select GA.GA_Name from GEOAREA GA where GA.GA_ID = EXTRACTVALUE(GEOAREA.HIERARCHY, '/structure/lga')) from GEOAREA where GA_ID = COMMUNITY.FK_GEOAREA) CommunityLGA    
                        FROM COMMUNITY ";

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var total = dbConn.Query<int>(@"SELECT count(1) from community").First();

                query = AddOrderToQueryCommunity(query, orderby, reverse);

                var result = dbConn.Query<Community>(query).Skip(page * perpage).Take(perpage).ToList();

                return new CommunityWrap
                {
                    Communities = result,
                    CurrentPage = 1,
                    NumberOfPages = total / perpage,
                    NumberPerPage = perpage,
                    Total = total
                };
            }
        }

        public Community GetCommunity(int id, bool withMembers)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var community = dbConn.Query<Community>(@"select COMMUNITY_ID, COMMUNITY_NAME, COMMUNITY_DESC, g.ga_id as FkGeoarea, g.fk_parent_id as stateid from community c 
                                            left join geoarea g on c.fk_geoarea = g.ga_id where community_id = :id", new { id })
                    .SingleOrDefault();

                if (community == null)
                    return null;

                if (withMembers)
                {
                    community.Members = dbConn.Query<Member>(@"SELECT MEMBER_ID, FIRST_NAME, SURNAME, DATE_OF_BIRTH,
                        (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = MEMBER.LKP_GENDER) as Gender, GRADE,
                        (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = MEMBER.LKP_STATUS) as STATUS                       
                        FROM MEMBER
                        WHERE MEMBER.FK_COMMUNITY = :id and LKP_MEMBER_TYPE = 78
                        order by first_name desc", new { id }).ToList();
                }

                return community;
            }
        }

        public Community GetTemporaryCommunity(int id, bool withMembers)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var community = dbConn.Query<Community>("select COMMUNITY_ID, COMMUNITY_NAME, COMMUNITY_DESC, g.fk_parent_id as stateid from community c left join geoarea g on c.fk_geoarea = g.ga_id where community_id = :id", new { id })
                    .SingleOrDefault();

                if (community == null)
                    return null;

                if (withMembers)
                {
                    community.Members = dbConn.Query<Member>(@"SELECT MEMBER_ID, FIRST_NAME, SURNAME, DATE_OF_BIRTH,
                        (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = MEMBER.LKP_GENDER) as Gender, GRADE,
                        (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = MEMBER.LKP_STATUS) as STATUS                       
                        FROM TMP_MEMBER
                        WHERE MEMBER.FK_COMMUNITY = :id and LKP_MEMBER_TYPE = 78
                        order by first_name desc", new { id }).ToList();
                }

                return community;
            }
        }

        public int AddCommunity(Community community, int operatorId)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var result = dbConn.Execute("insert into community (community_name, community_Desc, lkp_status, fk_geoarea, sys_reg_user, sys_active, sys_delete, sys_edit_user) values (:community_name, :community_Desc, :lkp_status, :fk_geoarea, :sys_reg_user, 1, 0, :sys_edit_user)",
                    new
                    {
                        community_name = community.CommunityName,
                        community_Desc = community.CommunityDesc,
                        lkp_status = (int)EnumLookups.CommunityStatusCreated,
                        fk_geoarea = community.FkGeoarea,
                        sys_reg_user = operatorId,
                        sys_edit_user = operatorId
                    });

                if (result == 1)
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.AddCommunity,
                        Description = EnumAuditEvents.AddCommunity.GetEnumDescription() + $" {community.CommunityName}",
                        TimeStamp = DateTime.Now,
                        UserId = operatorId
                    });

                return result;
            }
        }

        public int EditCommunity(Community community, int operatorId)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var result = dbConn.Execute("update community set community_name = :community_name, community_desc = :community_Desc, fk_geoarea =:fk_geoarea, lkp_status = :lkp_status, " +
                                            "sys_edit_user = :sys_edit_user, sys_delete = :sys_delete where community_id = :community_id",
                    new
                    {
                        community_id = community.CommunityId,
                        community_name = community.CommunityName,
                        community_Desc = community.CommunityDesc,
                        lkp_status = community.LkpStatus,
                        fk_geoarea = community.FkGeoarea,
                        sys_delete = community.Deactivated ? 1 : 0,
                        sys_edit_user = operatorId
                    });

                if (result == 1)
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.EditCommunity,
                        Description = EnumAuditEvents.AddCommunity.GetEnumDescription() + $" {community.CommunityName}",
                        TimeStamp = DateTime.Now,
                        UserId = operatorId,
                        ItemId = community.CommunityId
                    });

                return result;
            }
        }

        public CommunityWrap SearchCommunities(SearchParam search)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                var query = @"SELECT COMMUNITY_ID, 
                        COMMUNITY_NAME, 
                        COMMUNITY_DESC,
                        COMMUNITY.SYS_REG_dATE,
                        (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = COMMUNITY.LKP_STATUS) as LKP_STATUS, 
                        MHA_CODE,
                        (select (select GA.GA_Name from GEOAREA GA where GA.GA_ID = EXTRACTVALUE(GEOAREA.HIERARCHY, '/structure/state')) from GEOAREA where GA_ID = COMMUNITY.FK_GEOAREA) CommunityState,
                        (select (select GA.GA_Name from GEOAREA GA where GA.GA_ID = EXTRACTVALUE(GEOAREA.HIERARCHY, '/structure/lga')) from GEOAREA where GA_ID = COMMUNITY.FK_GEOAREA) CommunityLGA    
                        FROM COMMUNITY left join geoarea g on COMMUNITY.fk_geoarea = g.ga_id where community_id is not null ";

                var param = new DynamicParameters();

                if (!string.IsNullOrEmpty(search.SearchName))
                {
                    query += " and lower(COMMUNITY_NAME) like :name ";
                    param.Add("name", "%" + search.SearchName.ToLower() + "%");
                }

                if (!string.IsNullOrEmpty(search.SearchLga))
                {
                    query += " and g.ga_id = :lga ";
                    param.Add("lga", search.SearchLga);
                }

                if (!string.IsNullOrEmpty(search.SearchState))
                {
                    query += " and g.FK_PARENT_ID = :state ";
                    param.Add("state", search.SearchState);
                }

                if (!string.IsNullOrEmpty(search.SearchStartDate))
                {
                    var startdate = DateTime.Parse(search.SearchStartDate);
                    query += " and community.sys_reg_date >= :startdate ";
                    param.Add("startdate", startdate);
                }

                if (!string.IsNullOrEmpty(search.SearchEndDate))
                {
                    var enddate = DateTime.Parse(search.SearchEndDate).AddDays(1);
                    query += " and community.sys_reg_date < :enddate ";
                    param.Add("enddate", enddate);
                }

                query = AddOrderToQueryCommunity(query, search.OrderBy, int.Parse(search.Reverse));

                var communities = dbConn.Query<Community>(query, param).ToList();

                var page = communities.Skip(search.Page * search.PerPage).Take(search.PerPage).ToList();

                if (search.IsPrint == 1 && search.PrintPage == 0)
                    return new CommunityWrap
                    {
                        Communities = communities,
                        CurrentPage = 1,
                        NumberOfPages = 1,
                        NumberPerPage = communities.Count,
                        Total = communities.Count
                    };

                return new CommunityWrap
                {
                    Communities = page,
                    CurrentPage = 1,
                    NumberOfPages = communities.Count / search.PerPage,
                    NumberPerPage = search.PerPage,
                    Total = communities.Count
                };
            }
        }

        private string AddOrderToQueryCommunity(string query, string orderby, int reverse)
        {
            if (reverse == 1)
            {
                switch (orderby)
                {
                    case "Name":
                        query += "order by community_name desc";
                        break;
                    case "Lga":
                        query += "order by CommunityLGA desc";
                        break;
                    case "State":
                        query += "order by CommunityState desc";
                        break;
                    case "DateRegistered":
                        query += "order by sys_reg_date desc";
                        break;
                    default:
                        query += "order by community_name";
                        break;
                }
            }
            else
            {
                switch (orderby)
                {
                    case "Name":
                        query += "order by community_name";
                        break;
                    case "Lga":
                        query += "order by CommunityLGA";
                        break;
                    case "State":
                        query += "order by CommunityState";
                        break;
                    case "DateRegistered":
                        query += "order by sys_reg_date";
                        break;
                    default:
                        query += "order by community_name desc";
                        break;
                }
            }

            return query;
        }

        private GraphData GetCommunityLastYear()
        {
            var graph = new GraphData();

            var yearletter =
                @"select d.x, NVL(amt.y,0) as y
                    from
                    (SELECT month_value, SUBSTR(month_display, 1, 3) as x FROM WWV_FLOW_MONTHS_MONTH) d
                    left join (select SUBSTR(TO_CHAR(sys_reg_date, 'Mon'), 1, 3) as x, Count(*) as y
                    from community
                    where sys_reg_date between trunc (sysdate, 'yy') AND SYSDATE
                    Group by SUBSTR(TO_CHAR(sys_reg_date, 'Mon'), 1, 3)) amt
                    on d.x = amt.x
                    order by d.month_value";

            using (
                var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                var graphdata = dbConn.Query(yearletter).ToList();

                foreach (var data in graphdata)
                {
                    graph.X.Add(data.X.ToString());
                    graph.Y.Add(int.Parse(data.Y.ToString()));
                }
            }

            return graph;
        }


        // Members
        public MemberWrap GetMembers(int perpage, int page, string orderby, int reverse, string communityId, bool isTemp)
        {
            var table = isTemp ? "tmp_member" : "member";
            var query = $@"select member_id, first_name, surname, date_of_birth, l.LKP_VALUE as Gender, l.LKP_VALUE as Gender, l2.LKP_VALUE as status,
                    c.community_name as communityname, g.ga_name as lga, g2.ga_name as state
                    from {table} m 
                    left join community c on m.fk_community = c.community_id
                    left join geoarea g on g.ga_id = c.fk_geoarea
                    left join geoarea g2 on g2.ga_id = g.fk_parent_id
                    left join lookups l on m.lkp_gender = l.unqkey
                    left join lookups l2 on m.LKP_STATUS = l2.unqkey
                    where lkp_member_type = 78 ";

            if (!string.IsNullOrEmpty(communityId))
                query += $"and community_id = {communityId} ";

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var total = dbConn.Query<int>(@"SELECT count(1) from member").First();

                query = AddOrderToQueryMember(query, orderby, reverse);

                var result = dbConn.Query<Member>(query).Skip(page * perpage).Take(perpage).ToList();

                return new MemberWrap
                {
                    Members = result,
                    CurrentPage = 1,
                    NumberOfPages = total / perpage,
                    NumberPerPage = perpage,
                    Total = total
                };
            }
        }

        public MemberWrap SearchMembers(SearchParam search)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                var query = @"select member_id, first_name, surname, date_of_birth, l.LKP_VALUE as Gender, (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = m.LKP_STATUS) as STATUS,
                    c.community_name as communityname, g.ga_name as lga, (select ga_name from geoarea where ga_id = g.fk_parent_id) as state
                    from member m left join community c on m.fk_community = c.community_id
                    left join geoarea g on g.ga_id = c.fk_geoarea
                    left join lookups l on m.lkp_gender = l.unqkey
                    where lkp_member_type = 78 ";

                var param = new DynamicParameters();

                if (!string.IsNullOrEmpty(search.Firstname))
                {
                    query += " and lower(first_name) like :firstname ";
                    param.Add("firstname", "%" + search.Firstname.ToLower() + "%");
                }

                if (!string.IsNullOrEmpty(search.Surname))
                {
                    query += " and lower(surname) like :surname ";
                    param.Add("surname", "%" + search.Surname.ToLower() + "%");
                }

                if (!string.IsNullOrEmpty(search.SearchCommunity))
                {
                    query += " and community_id = :commid ";
                    param.Add("commid", search.SearchCommunity);
                }

                if (!string.IsNullOrEmpty(search.SearchGender))
                {
                    query += " and l.LKP_VALUE = :gender ";
                    param.Add("gender", search.SearchGender);
                }

                if (!string.IsNullOrEmpty(search.SearchLga))
                {
                    query += " and g.ga_id = :lga ";
                    param.Add("lga", search.SearchLga);
                }

                if (!string.IsNullOrEmpty(search.SearchState))
                {
                    query += " and g.FK_PARENT_ID = :state ";
                    param.Add("state", search.SearchState);
                }

                if (!string.IsNullOrEmpty(search.SearchStartDate))
                {
                    var startdate = DateTime.Parse(search.SearchStartDate);
                    query += " and m.registration_date >= :startdate ";
                    param.Add("startdate", startdate);
                }

                if (!string.IsNullOrEmpty(search.SearchEndDate))
                {
                    var enddate = DateTime.Parse(search.SearchEndDate).AddDays(1);
                    query += " and m.registration_date < :enddate ";
                    param.Add("enddate", enddate);
                }

                if (string.IsNullOrEmpty(search.Reverse))
                    search.Reverse = "1";
                query = AddOrderToQueryMember(query, search.OrderBy, int.Parse(search.Reverse));

                var members = dbConn.Query<Member>(query, param).ToList();

                var page = members.Skip(search.Page * search.PerPage).Take(search.PerPage).ToList();

                if (search.IsPrint == 1 && search.PrintPage == 0)
                    return new MemberWrap
                    {
                        Members = members,
                        CurrentPage = 1,
                        NumberOfPages = 1,
                        NumberPerPage = members.Count,
                        Total = members.Count
                    };

                return new MemberWrap
                {
                    Members = page,
                    CurrentPage = 1,
                    NumberOfPages = members.Count / search.PerPage,
                    NumberPerPage = search.PerPage,
                    Total = members.Count
                };
            }
        }

        public List<Member> GetAllMembers(int memberType)
        {
            var query =
                @"select MEMBER_ID, FIRST_NAME, SURNAME, DATE_OF_BIRTH, (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = m.LKP_GENDER) as Gender, (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = m.LKP_STATUS) as STATUS,
                    c.community_name as communityname, g.ga_name as lga, (select ga_name from geoarea where ga_id = g.fk_parent_id) as state
                    from member m left join community c on m.fk_community = c.community_id
                    left join geoarea g on g.ga_id = c.fk_geoarea
                    where lkp_member_type = :memberType order by m.sys_reg_date";

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn
                    .Query<Member>(query, new { memberType }).ToList();
            }
        }

        public List<Member> GetAllTemporaryMembers(int memberType)
        {
            var query =
                @"select MEMBER_ID, FIRST_NAME, SURNAME, DATE_OF_BIRTH, (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = m.LKP_GENDER) as Gender, (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = m.LKP_STATUS) as STATUS,
                    c.community_name as communityname, g.ga_name as lga, (select ga_name from geoarea where ga_id = g.fk_parent_id) as state
                    from tmp_member m left join community c on m.fk_community = c.community_id
                    left join geoarea g on g.ga_id = c.fk_geoarea
                    where lkp_member_type = :memberType order by m.sys_reg_date";

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn
                    .Query<Member>(query, new { memberType }).ToList();
            }
        }

        public List<Member> GetDuplicateMembers(int memberType)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<Member>(@"SELECT * FROM (SELECT member_id, first_name, surname, lkp_gender, date_of_birth, registration_date, lkp_mar_status, count(*) over (partition by first_name, surname, date_of_birth) cnt
                                                FROM member where lkp_member_type = :memberType) WHERE cnt > 1", new {memberType}).ToList();
            }
        }

        public List<Member> GetMembersThisMonth(int memberType, bool justDate)
        {
            string query;

            if (justDate)
                query =
                    "select sys_reg_date from member where lkp_member_type = :memberType and sys_reg_date between trunc (sysdate, 'mm') and sysdate order by sys_reg_date";
            else
                query =
                    @"select MEMBER_ID, FIRST_NAME, SURNAME, DATE_OF_BIRTH, (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = m.LKP_GENDER) as Gender, (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = m.LKP_STATUS) as STATUS,
                        c.community_name as communityname, g.ga_name as lga, (select ga_name from geoarea where ga_id = g.fk_parent_id) as state
                        from member m left join community c on m.fk_community = c.community_id
                        left join geoarea g on g.ga_id = c.fk_geoarea
                        where lkp_member_type = :memberType and m.sys_reg_date between trunc (sysdate, 'mm') and sysdate order by m.sys_reg_date";


            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn
                    .Query<Member>(query, new { memberType }).ToList();
            }
        }

        public List<Member> GetTemporaryMembersThisMonth(int memberType, bool justDate)
        {
            string query;

            if (justDate)
                query =
                    "select sys_reg_date from tmp_member where lkp_member_type = :memberType and sys_reg_date between trunc (sysdate, 'mm') and sysdate order by sys_reg_date";
            else
                query =
                    @"SELECT MEMBER_ID, FIRST_NAME, SURNAME, DATE_OF_BIRTH, (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = m.LKP_GENDER) as Gender, (select LKP_VALUE from LOOKUPS where LOOKUPS.UNQKEY = m.LKP_STATUS) as STATUS,
                        c.community_name as communityname, g.ga_name as lga, (select ga_name from geoarea where ga_id = g.fk_parent_id) as state
                        from tmp_member m left join community c on m.fk_community = c.community_id
                        left join geoarea g on g.ga_id = c.fk_geoarea
                        where lkp_member_type = :memberType and m.sys_reg_date between trunc (sysdate, 'mm') and sysdate order by m.sys_reg_date";


            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn
                    .Query<Member>(query, new { memberType }).ToList();
            }
        }

        private GraphData GetMembersLastYear(int memberType)
        {
            var graph = new GraphData();
            var yearnumber =
                @"select d.x, NVL(amt.y,0) as y
                    from
                    (SELECT month_value as x FROM WWV_FLOW_MONTHS_MONTH) d
                    left join (select extract(month from sys_reg_date) as x, Count(*) as y
                    from member
                    where lkp_member_type = 78 and sys_reg_date between trunc (sysdate, 'yy') AND SYSDATE
                    Group by extract(month from sys_reg_date)
                    order by x) amt
                    on d.x = amt.x
                    order by d.x";

            var yearletter =
                @"select d.x, NVL(amt.y,0) as y
                    from
                    (SELECT month_value, SUBSTR(month_display, 1, 3) as x FROM WWV_FLOW_MONTHS_MONTH) d
                    left join (select SUBSTR(TO_CHAR(sys_reg_date, 'Mon'), 1, 3) as x, Count(*) as y
                    from member
                    where lkp_member_type = 78 and sys_reg_date between trunc (sysdate, 'yy') AND SYSDATE
                    Group by SUBSTR(TO_CHAR(sys_reg_date, 'Mon'), 1, 3)) amt
                    on d.x = amt.x
                    order by d.month_value";

            using (
                var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                var graphdata = dbConn.Query(yearletter).ToList();

                foreach (var data in graphdata)
                {
                    graph.X.Add(data.X.ToString());
                    graph.Y.Add(int.Parse(data.Y.ToString()));
                }
            }

            return graph;
        }

        private string AddOrderToQueryMember(string query, string orderby, int reverse)
        {
            if (reverse == 1)
            {
                switch (orderby)
                {
                    case "Firstname":
                        query += "order by m.first_name desc";
                        break;
                    case "Surname":
                        query += "order by m.surname desc";
                        break;
                    case "Gender":
                        query += "order by l.lkp_value desc";
                        break;
                    case "Community":
                        query += "order by c.community_name desc";
                        break;
                    case "Lga":
                        query += "order by lga desc";
                        break;
                    case "State":
                        query += "order by state desc";
                        break;
                    case "DateRegistered":
                        query += "order by registration_date";
                        break;
                    default:
                        query += "order by first_name";
                        break;
                }
            }
            else
            {
                switch (orderby)
                {
                    case "Firstname":
                        query += "order by m.first_name";
                        break;
                    case "Surname":
                        query += "order by m.surname";
                        break;
                    case "Gender":
                        query += "order by l.lkp_value";
                        break;
                    case "Community":
                        query += "order by c.community_name";
                        break;
                    case "Lga":
                        query += "order by lga";
                        break;
                    case "State":
                        query += "order by state";
                        break;
                    case "DateRegistered":
                        query += "order by registration_date desc";
                        break;
                    default:
                        query += "order by first_name desc";
                        break;
                }
            }

            return query;
        }

        private GraphData GetMembersThisMonth(int memberType)
        {
            var graph = new GraphData();
            var query =
                @"  select d.x, NVL(amt.y,0) as y from
                    (SELECT (EXTRACT(month from sysdate) + LEVEL - 1) - 1 AS x --TRUNC(SYSDATE, 'MM') + LEVEL - 1 AS day
                    FROM dual
                    CONNECT BY TRUNC(TRUNC(SYSDATE, 'MM') + LEVEL - 1, 'MM') = TRUNC(SYSDATE, 'MM')) d
                    left join (select extract(day from sys_reg_date) as x, Count(*) as y
                    from member
                    where lkp_member_type = 78 and sys_reg_date between trunc (sysdate, 'mm') AND SYSDATE
                    Group by extract(day from sys_reg_date)
                    order by x) amt
                    on d.x = amt.x
                    order by d.x";

            using (
                var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                var graphdata = dbConn.Query(query).ToList();

                foreach (var data in graphdata)
                {
                    graph.X.Add(data.X.ToString());
                    graph.Y.Add(int.Parse(data.Y.ToString()));
                }
            }

            return graph;
        }


        private void AddToAudit(Audit audit)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                dbConn.Execute("insert into Audits (eventid, timestamp, userid, itemid, description) values (:eventid, :timestamp, :userid, :itemid, :description)",
                    new
                    {
                        audit.EventId,
                        audit.TimeStamp,
                        audit.UserId,
                        audit.ItemId,
                        audit.Description
                    });
            }
        }
    }
}