﻿using Dapper;
using Iship.Enums;
using Iship.Extensions;
using Iship.Models;
using Iship.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Iship.Services
{
    public class AuthService
    {
        private readonly string _conn = ConfigurationManager.ConnectionStrings["Iship"].ConnectionString;

        public int GetNumberOfUsers()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<int>("select count(*) from member").First();
            }
        }

        public ApplicationUser GetUserById(int id)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ApplicationUser>("select * from operator where id = :id", new { id }).FirstOrDefault();
            }
        }

        public ApplicationUser GetUserByEmail(string email)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ApplicationUser>("select * from operator where email = :email", new { email }).FirstOrDefault();
            }
        }

        public ApplicationUser GetUserByUsername(string username)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ApplicationUser>("select * from operator where username = :username", new { username }).FirstOrDefault();
            }
        }

        public List<ApplicationUser> GetUsers()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ApplicationUser>("select * from operator").ToList();
            }
        }

        public List<ApplicationUser> GetDisabledUsers()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                    return dbConn.Query<ApplicationUser>("select * where lockoutenabled = 1").ToList();
            }
        }

        public UserClaim GetClaim(int id)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<UserClaim>("select * from memberclaim where id = :id", new { id }).FirstOrDefault();
            }
        }

        public List<UserClaim> GetOperatorClaimsList()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<UserClaim>("select * from ListOperatorClaims").ToList();
            }
        }

        public ViewAudits GetAuditTrails(int perpage, int page, string orderby, int reverse)
        {
            var query = @"SELECT a.*, m.*, e.*
                        FROM audits a
                        INNER JOIN listauditevents e on e.id = a.eventid
                        INNER JOIN operator m on m.id = a.userid ";

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var total = dbConn.Query<int>("select count(id) from audits").First();

                List<Audit> result;
                if (reverse == 1)
                {
                    switch (orderby)
                    {
                        case "FirstName":
                            query += "order by m.firstname desc";
                            result = GetAudits(query, dbConn, page, perpage);
                            break;
                        case "EventName":
                            query += "order by e.name desc";
                            result = GetAudits(query, dbConn, page, perpage);
                            break;
                        case "TimeStamp":
                            query += "order by a.timestamp desc";
                            result = GetAudits(query, dbConn, page, perpage);
                            break;
                        default:
                            query += "order by e.timestamp desc";
                            result = GetAudits(query, dbConn, page, perpage);
                            break;
                    }
                }
                else
                {
                    switch (orderby)
                    {
                        case "FirstName":
                            query += "order by m.firstname";
                            result = GetAudits(query, dbConn, page, perpage);
                            break;
                        case "EventName":
                            query += "order by e.name";
                            result = GetAudits(query, dbConn, page, perpage);
                            break;
                        case "TimeStamp":
                            query += "order by a.timestamp";
                            result = GetAudits(query, dbConn, page, perpage);
                            break;
                        default:
                            query += "order by e.timestamp";
                            result = GetAudits(query, dbConn, page, perpage);
                            break;
                    }
                }

                return new ViewAudits
                {
                    Audits = result,
                    CurrentPage = 1,
                    NumberOfPages = total / perpage,
                    NumberPerPage = perpage,
                    Total = total
                };
            }
        }

        private List<Audit> GetAudits(string query, OracleConnection dbConn, int page, int perpage)
        {
            return dbConn.Query<Audit, ApplicationUser, AuditEvent, Audit>(
                    query,
                    (audit, user, ev) =>
                    {
                        audit.Event = new AuditEvent
                        {
                            Description = ev.Description,
                            Id = ev.Id,
                            Name = ev.Name
                        };

                        audit.User = new ApplicationUser
                        {
                            FirstName = user.FirstName,
                            Surname = user.Surname,
                            Id = user.Id
                        };

                        return audit;
                    },
                    splitOn: "id")
                .Distinct()
                .Skip(page * perpage)
                .Take(perpage).ToList();
        }

        public ViewAudits SearchAuditTrails(int perpage, string userid, string action, string start, string end, string formno,
            string docno)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                var query = @"SELECT a.*, m.*, e.*
                        FROM audits a
                        INNER JOIN listauditevents e on e.id = a.eventid
                        INNER JOIN member m on m.id = a.userid where a.id is not null";

                var param = new Dictionary<string, string>();

                if (!string.IsNullOrEmpty(userid))
                {
                    query += " and a.userid = :userid";
                    param.Add("userid", userid);
                }

                if (!string.IsNullOrEmpty(action))
                {
                    query += " and e.Name = :name";
                    param.Add("name", action);
                }

                if (!string.IsNullOrEmpty(formno))
                {
                    query += " and a.formno = :formno";
                    param.Add("formno", formno);
                }

                if (!string.IsNullOrEmpty(docno))
                {
                    query += " and a.docno = :docno";
                    param.Add("docno", docno);
                }

                if (!string.IsNullOrEmpty(start))
                {
                    var startdate = DateTime.Parse(start);
                    query += " and a.timestamp >= :startdate";
                    param.Add("startdate", startdate.ToShortDateString());
                }

                if (!string.IsNullOrEmpty(end))
                {
                    var enddate = DateTime.Parse(end).AddDays(1);
                    query += " and a.timestamp < :enddate";
                    param.Add("enddate", enddate.ToShortDateString());
                }

                var total = dbConn.Query<int>("select count(id) from audits").First();

                var dbParams = new DynamicParameters();
                foreach (var p in param)
                    dbParams.Add(p.Key, p.Value);

                var audits = dbConn.Query<Audit, ApplicationUser, AuditEvent, Audit>(
                        query,
                        (audit, user, ev) =>
                        {
                            audit.Event = new AuditEvent
                            {
                                Description = ev.Description,
                                Id = ev.Id,
                                Name = ev.Name
                            };

                            audit.User = new ApplicationUser
                            {
                                FirstName = user.FirstName,
                                Surname = user.Surname,
                                Id = user.Id
                            };

                            return audit;
                        },
                        dbParams,
                        splitOn: "id")
                    .Distinct().ToList();
                
                return new ViewAudits
                {
                    Audits = audits,
                    CurrentPage = 1,
                    NumberOfPages = total / perpage,
                    NumberPerPage = perpage,
                    Total = total
                };
            }
        }
        
        public int UpdateUserDetails(ApplicationUser user)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var appuser = GetUserById(user.Id);

                // if its a different username, confirm it is unique
                if (!string.Equals(appuser.UserName, user.UserName, StringComparison.CurrentCultureIgnoreCase))
                {
                    var exists = GetUserByUsername(user.UserName.ToLower());

                    if (exists != null)
                        return -1;
                }

                var result = dbConn.Execute("update operator set username = :username, phonenumber = :phonenumber where id = :id", new
                {
                    user.UserName,
                    user.PhoneNumber,
                    user.Id
                });

                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.PersonalDetailsUpdated,
                    Description = EnumAuditEvents.PersonalDetailsUpdated.GetEnumDescription(),
                    TimeStamp = DateTime.Now,
                    UserId = user.Id
                });

                return result;
            }
        }

        public int UpdateUserPassword(ApplicationUser user)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                // add audit
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.PasswordChanged,
                    Description = EnumAuditEvents.PasswordChanged.GetEnumDescription(),
                    TimeStamp = DateTime.Now,
                    UserId = user.Id
                });

                return 1;
            }
        }

        public int AddClaimsToUser(int userId, string claims, int operatorId)
        {
            var claimslist = new List<UserClaim>();
            var commavalues = new List<string>();

            foreach (string claim in claims.Split(','))
            {
                var type = claim.Split('/')[0].Trim();
                var value = claim.Split('/')[1].Trim();
                commavalues.Add(value);

                claimslist.Add(new UserClaim { Action = value, Controller = type });
            }

            // confirm user doesn't have these claims (yeah, shouldn't be necessary but getting some funny double postings)
            var userclaims = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().GetClaims(userId);
            var exists = claimslist.Where(x => userclaims.Any(y => y.Value == x.Action && y.Type == x.Controller));
            var newlist = claimslist.Except(exists).ToList();

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                foreach (var claim in newlist)
                {
                    dbConn.Execute(
                        "insert into operatorclaim (operatorid, claimtype, claimvalue) values (:operatorid, :claimtype, :claimvalue)",
                        new
                        {
                            operatorid = userId,
                            claimtype = claim.Controller,
                            claimvalue = claim.Action
                        });
                }
            }

            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);

            foreach (var part in parts)
            {
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.PrivilegeAssigned,
                    Description = EnumAuditEvents.PrivilegeAssigned.GetEnumDescription() + $" - { string.Join(", ", part)}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId
                });
            }

            return 1;
        }

        public int RemoveClaimsFromUser(int userId, string claims, int operatorId)
        {
            var claimslist = new List<UserClaim>();
            var commavalues = new List<string>();

            foreach (string claim in claims.Split(','))
            {
                var type = claim.Split('/')[0].Trim();
                var value = claim.Split('/')[1].Trim();
                commavalues.Add(value);

                claimslist.Add(new UserClaim { Action = value, Controller = type });
            }

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                foreach (var claim in claimslist)
                {
                    dbConn.Execute(
                        "delete from operatorclaim where claimtype = :claimtype and claimvalue = :claimvalue and operatorid = :operatorid",
                        new
                        {
                            claimtype = claim.Controller,
                            claimvalue = claim.Action,
                            operatorid = userId
                        });
                }
            }

            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);

            foreach (var part in parts)
            {
                AddToAudit(new Audit
                {
                    EventId = (int)EnumAuditEvents.PrivilegeRevoked,
                    Description = EnumAuditEvents.PrivilegeRevoked.GetEnumDescription() + $" - { string.Join(", ", part)}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId
                });
            }

            return 1;
        }

        public int UserLoggedOut(int userId)
        {
            AddToAudit(new Audit()
            {
                EventId = (int)EnumAuditEvents.LogOut,
                Description = EnumAuditEvents.LogOut.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = userId
            });
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Execute("insert into Audits (eventid, timestamp, userid) values (:eventid, :timestamp, :userid)",
                    new
                    {
                        EventId = (int)EnumAuditEvents.LogOut,
                        Description = EnumAuditEvents.LogOut.GetEnumDescription(),
                        TimeStamp = DateTime.Now,
                        UserId = userId
                    });
            }
        }

        public int UserLoggedIn(int userId)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Execute("insert into Audits (eventid, timestamp, userid) values (:eventid, :timestamp, :userid)", 
                    new
                    {
                        EventId = (int)EnumAuditEvents.LoginSuccess,
                        Description = EnumAuditEvents.LoginSuccess.GetEnumDescription(),
                        TimeStamp = DateTime.Now,
                        UserId = userId
                    });
            }
        }

        public int UserLoginFailed(int userId)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Execute("insert into Audits (eventid, timestamp, userid) values (:eventid, :timestamp, :userid)",
                    new
                    {
                        EventId = (int)EnumAuditEvents.LoginFailed,
                        Description = EnumAuditEvents.LoginSuccess.GetEnumDescription(),
                        TimeStamp = DateTime.Now,
                        UserId = userId
                    });
            }
        }

        public bool CheckEmailRegistered(string email)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var result = dbConn.Query<ApplicationUser>("select id from operator where email = :email", new { email }).SingleOrDefault();

                return result != null;
            }
        }

        public int CreateOperator(ApplicationUser opr, string hashedPassword, int oprType, int operatorId)
        {
            // check if username is already in use
            string username = GetUniqueUsername(opr.Email.Split('@')[0]);

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                int id = dbConn.Query<int>("select operator_seq.nextval from dual").Single();
                
                var result = dbConn.Execute(
                    "insert into operator (id, firstname, surname, username, gender, email, passwordhash, securitystamp, phonenumber, operatortype) " +
                    "values (:id, :firstname, :surname, :username, :gender, :email, :passwordhash, :securitystamp, :phonenumber, :operatortype)",
                    new
                    {
                        id,
                        firstname = opr.FirstName,
                        surname = opr.Surname,
                        gender = opr.Gender,
                        email = opr.Email,
                        username,
                        passwordhash = hashedPassword,
                        securitystamp = Guid.NewGuid().ToString(),
                        phonenumber = opr.PhoneNumber,
                        operatortype = oprType
                    });

                if (result != 0)
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.AddCommunityOperator,
                        Description = EnumAuditEvents.AddCommunityOperator.GetEnumDescription(),
                        TimeStamp = DateTime.Now,
                        UserId = operatorId,
                        ItemId = id
                    });

                return result;
            }
        }

        private string GetUniqueUsername(string username)
        {
            while (true)
            {
                // recursively check till no user exists
                using (var dbConn = new OracleConnection(_conn))
                {
                    dbConn.Open();

                    var duplicate = dbConn.Query<int>("select count(1) from operator where username = :username", new  {username}).First();

                    if (duplicate == 0)
                        return username;

                    username = username + duplicate;
                }
            }
        }

        public List<ApplicationUser> GetOperators(List<int> oprType, int oprid)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var result = dbConn.Query<ApplicationUser>($"select o.*, t.isadmin, t.name as operatortypename from operator o left join operatortype t on t.id = o.operatortype where o.id != :oprid and operatortype in ({string.Join(",", oprType)})",
                    new
                    {
                        oprid
                    }).ToList();

                return result;
            }
        }

        public int SetOperatorAsAdmin(int id, int status, int oprid)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var result = dbConn.Execute("update operator set operatortype = :operatortype where id = :id",
                    new
                    {
                        id,
                        operatortype = status == 0 ? 3 : 2
                    });

                if (result == 1)
                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.UpdateOperatorAdminStatus,
                        Description = EnumAuditEvents.UpdateOperatorAdminStatus.GetEnumDescription() + " - " + status == "0" ? "Remove Admin" : "Make Admin",
                        TimeStamp = DateTime.Now,
                        UserId = oprid,
                        ItemId = id
                    });

                return result;
            }
        }

        public int UpdateFirstTime(string id)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var result = dbConn.Execute("update operator set isfirsttimelogin = 0, email_confirmed = 1 where id = :id",
                    new
                    {
                        id
                    });

                if (result == 1)
                {
                    // Add default claims
                    AddClaimsToUser(int.Parse(id), "UserManagement/MyAccount", 0);

                    AddToAudit(new Audit
                    {
                        EventId = (int)EnumAuditEvents.PasswordChanged,
                        Description = EnumAuditEvents.PasswordChanged.GetEnumDescription() + " - First time login",
                        TimeStamp = DateTime.Now,
                        UserId = int.Parse(id),
                        ItemId = int.Parse(id)
                    });
                }

                return result;
            }
        }

        public int SetOperatorStatus(int id, int status, int oprid)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var result = dbConn.Execute("update operator set lockoutenabled = :status where id = :id",
                    new
                    {
                        id,
                        status
                    });

                if (result == 1)
                {
                    int eventid = status == 1
                        ? (int)EnumAuditEvents.EnableOperator
                        : (int)EnumAuditEvents.DisableOperator;
                    AddToAudit(new Audit
                    {
                        EventId = eventid,
                        Description = EnumAuditEvents.UpdateOperatorAdminStatus.GetEnumDescription(),
                        TimeStamp = DateTime.Now,
                        UserId = oprid,
                        ItemId = id
                    });
                }

                return result;
            }
        }

        public List<Section> GetSections()
        {
            var result = new List<Section>();
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                dbConn.Query<Section, SubSection, Section>(@"
                    SELECT s.*, c.*
                    FROM section s
                    LEFT JOIN subsection c ON c.sectionid = s.Id                    
                    ", (section, subsection) =>
                    {
                        var selected = result.SingleOrDefault(x => x.Id == section.Id);

                        if (selected == null)
                            result.Add(section);
                        else
                            section = selected;

                        if (subsection != null)
                            section.SubSections.Add(subsection);

                        return section;
                    });

                return result;
            }
        }

        public List<UserClaim> GetUserClaims()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<UserClaim>("select * from listoperatorclaims").ToList();
            }
        }

        public List<UserClaim> GetUserClaims(int oprid)
        {
            var claims = new List<UserClaim>();

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                var oprclaims = dbConn.Query("select o.*, l.visible, l.subsectionid from operatorclaim o left join listoperatorclaims l on " +
                                             "l.action = o.claimvalue and l.controller = o. claimtype where operatorid = :oprid", new { oprid }).ToList();

                foreach (var claim in oprclaims)
                {
                    claims.Add(new UserClaim
                    {
                        Action = claim.CLAIMVALUE,
                        Controller = claim.CLAIMTYPE,
                        Visible = claim.VISIBLE == 1,
                        SubSectionId = claim.SUBSECTIONID == null ? 0 : int.Parse(claim.SUBSECTIONID.ToString())
                    });
                }

                return claims;
            }
        }

        public int SaveError(Exception ex, string url, string userId)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                // get browser and machineip if available
                var browser = HttpContext.Current.Request.Browser.Browser + " | Version: " + HttpContext.Current.Request.Browser.Version;
                var machinenameorip = HttpContext.Current.Request.GetOwinContext().Request.RemoteIpAddress;

                dbConn.Execute("insert into exception_log (exception_log_id, exceptiondate, url, message, stacktrace, machinenameorip, innermessage, innerstacktrace, browser, operatorid) values (" +
                               "exception_log_sequence.nextval, :exceptiondate, :url, :message, :stacktrace, :machinenameorip, :innermessage, :innerstacktrace, :browser, :operatorid)",
                    new
                    {
                        userId,
                        exceptiondate = DateTime.Now,
                        innermessage = ex.ToString(),
                        innerstacktrace = ex.InnerException?.StackTrace,
                        browser,
                        url,
                        ex.Message,
                        ex.StackTrace,
                        machinenameorip,
                        operatorid = userId
                    });

                return 1;
            }
        }

        public ViewErrors GetErrorLogs(int perpage, int page, string orderby, int reverse)
        {
            var query = @"SELECT e.*, m.firstname, m.surname
                        FROM exception_log e
                        INNER JOIN operator m on m.id = e.operatorid ";

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var total = dbConn.Query<int>("select count(exception_log_id) from exception_log").First();

                List<ErrorLog> result;
                if (reverse == 1)
                {
                    switch (orderby)
                    {
                        case "User":
                            query += "order by m.firstname desc";
                            result = GetErrorList(query, dbConn, page, perpage);
                            break;
                        default:
                            query += "order by e.exceptiondate desc";
                            result = GetErrorList(query, dbConn, page, perpage);
                            break;
                    }
                }
                else
                {
                    switch (orderby)
                    {
                        case "User":
                            query += "order by m.firstname";
                            result = GetErrorList(query, dbConn, page, perpage);
                            break;
                        default:
                            query += "order by e.exceptiondate";
                            result = GetErrorList(query, dbConn, page, perpage);
                            break;
                    }
                }

                return new ViewErrors
                {
                    ErrorLogs = result,
                    CurrentPage = 1,
                    NumberOfPages = total / perpage,
                    NumberPerPage = perpage,
                    Total = total
                };
            }
        }

        public ErrorLog GetErrorLog(int id)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ErrorLog>("select innermessage from exception_log where exception_log_id = :id", new { id }).FirstOrDefault();
            }
        }

        private List<ErrorLog> GetErrorList(string query, OracleConnection dbConn, int page, int perpage)
        {
            return dbConn.Query<ErrorLog>(query).ToList()
                .Skip(page * perpage)
                .Take(perpage).ToList();
        }

        private void AddToAudit(Audit audit)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                dbConn.Execute("insert into Audits (eventid, timestamp, userid) values (:eventid, :timestamp, :userid)",
                    new
                    {
                        audit.EventId,
                        audit.TimeStamp,
                        audit.UserId
                    });
            }
        }

        // Temp processes
        public void FixCommunities()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var communities = dbConn.Query<Community>("select * from community");
                int counter = 0;

                foreach (var comm in communities)
                {
                    try
                    {
                        if (string.IsNullOrEmpty(comm.GeoAreaDesc))
                            continue;

                        var lga = comm.GeoAreaDesc.Split('/')[0];
                        var state = comm.GeoAreaDesc.Split('/')[1];

                        var lgaid = dbConn.Query<string>(@"select g1.ga_id from geoarea g1 left join geoarea g2 on g1.fk_parent_id = g2.ga_id 
                                                    where lower(g1.ga_name) like :lga and lower(g2.ga_name) like :state", 
                            new { lga = "%" + lga.Trim().ToLower() + "%", state = "%" + state.Trim().ToLower() + "%" }).SingleOrDefault();

                        if (string.IsNullOrEmpty(lgaid))
                        {
                            // It might have been swapped so try again
                            lgaid = dbConn.Query<string>(@"select g1.ga_id from geoarea g1 left join geoarea g2 on g1.fk_parent_id = g2.ga_id 
                                                    where lower(g1.ga_name) like :lga and lower(g2.ga_name) like :state",
                                new { lga = "%" + lga.Trim().ToLower() + "%", state = "%" + state.Trim().ToLower() + "%" }).SingleOrDefault();
                        }

                        if (!string.IsNullOrEmpty(lgaid))
                        {
                            // then save the id
                            dbConn.Execute("update community set fk_geoarea = :lgaid where community_id = :id",
                                new { id = comm.CommunityId, lgaid });
                        }

                        counter++;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                    }
                }
               
            }
        }
    }
}
